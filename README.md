# 2D Strip-Packing Application

This application is written in [React](https://react.dev/) + [MUI](https://mui.com/).

Gitlab repository: https://gitlab.com/mkss_efalg/strip-packing

## Access Online

A pipeline and online gitlab page is implemented. Everytime new changes are pushed, the pipeline will run and deploy to the online gitlab page.

Online gitlab page: https://mkss_efalg.gitlab.io/strip-packing/

## Run locally

You will need to have [nodejs](https://nodejs.org/en) installed. With this you will have **npm** available. 

If preferred you can install **yarn** with **npm**: https://classic.yarnpkg.com/lang/en/docs/install/#windows-stable

In the project directory, you can run:

### `npm install` or `yarn install`

This will install all the dependencies that are defined in the package.json.

### `npm start` or `yarn start`

Runs the app in the development mode.\
Open [http://localhost:3000](http://localhost:3000) to view it in the browser.

The page will reload if you make edits.\
You will also see any lint errors in the console.

## Rough package structure

In `src/algorithms` are the implemented algorithms and everything that is directly related to them.

The classic split big rectangle into smaller ones is implemented in `src/components/randomGenerator/GetRandomRectanglesBySplitting.ts`.

In the root directory is a folder `ImportInstances`. In this folder are the files that can be imported => namely the zdf files, and one cgut file was converted to JSON, so it can be directly imported in the application with its import feature.

All other directories are for the react frontend.

## Manual

A rough description about the app can be found in the application itself => in the "About" tab. There is also a manual at the bottom of it.
