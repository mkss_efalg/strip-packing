import { AdvancedShelfType, MarryRectangle, RectangleType, ShelfType } from "../Algorithm.types"
import { deepCopy, getMaxWidth, sortDescendingHeight } from "../Algorithm.functions"

let storedContainerWidth: number
let storedContainerWidthHalf: number
let startTime
let endTime

export default function ReverseFit(containerWidth: number, rectangles: RectangleType[]) {
    if (rectangles.length == 0) {
        return {
            sortedRectangles: [],
            markers: [],
            error: "No input rectangles present.",
            runTime: undefined
        }
    }
    const rectanglesCopy = deepCopy(rectangles)
    if (getMaxWidth(rectanglesCopy) > containerWidth) {
        return {
            sortedRectangles: [],
            markers: [],
            error: "Rectangles do not fit into the strip container.",
            runTime: undefined
        }
    }

    storedContainerWidth = containerWidth
    storedContainerWidthHalf = storedContainerWidth / 2
    startTime = performance.now()
    const sortedRectangles = rectanglesCopy.sort(sortDescendingHeight)
    fitRectanglesIntoShelves(sortedRectangles)
    endTime = performance.now()
    return {
        sortedRectangles,
        markers: [],
        error: "",
        runTime: endTime - startTime
    }
}

function fitRectanglesIntoShelves(sortedRectangles: RectangleType[]) {
    const shelves: AdvancedShelfType[] = []
    fitRectanglesIntoShelf(sortedRectangles, shelves)
}

function fitRectanglesIntoShelf(sortedRectangles: RectangleType[], shelves: AdvancedShelfType[]) {
    const newSortedRectangles: RectangleType[] = []
    const h0leftBoundShelf: AdvancedShelfType = {
        posX: 0,
        posY: 0,
        width: storedContainerWidth,
        currentWidth: 0,
        currentHeight: 0,
        rectangles: []
    }
    let rectangleID: number = 0
    rectangleID = step1_putBoxWidthBiggerHalf(sortedRectangles, newSortedRectangles, h0leftBoundShelf, shelves, rectangleID)
    shelves.push(h0leftBoundShelf)
    let nextRectangleID: number = 0
    if (rectangleID < sortedRectangles.length) {
        h0leftBoundShelf.currentHeight = newSortedRectangles[0].height
        let step1ShelvesID: number = shelves.length
        nextRectangleID = step2_safeHighRectangleContainerSpace(newSortedRectangles, h0leftBoundShelf, shelves, rectangleID, step1ShelvesID)
        // //Step 3 end rectangles with nextfit on h0 shelves and above
        nextRectangleID = step3_ffdh(newSortedRectangles, shelves, nextRectangleID, rectangleID + nextRectangleID, step1ShelvesID)
    }
    if (rectangleID + nextRectangleID !== sortedRectangles.length) {
        console.debug("Missing rectacles to fill in container. " + nextRectangleID + "/" + sortedRectangles.length)
    }
}

/*
 * Step 1
 * */
function step1_putBoxWidthBiggerHalf(
    rectangles: RectangleType[],
    newRectangles: RectangleType[],
    h0shelf: ShelfType,
    shelves: AdvancedShelfType[],
    id: number
): number {
    rectangles.forEach((rectangle, index) => {
        if (rectangle.width > storedContainerWidth) {
            console.debug("Rectangle with width " + rectangle.width + " and heigth " + rectangle.height + " will never fit in the container. ID: " + index)
        } else if (rectangle.width > storedContainerWidthHalf) {
            //H0
            const newShelf: AdvancedShelfType = {
                posX: 0,
                posY: h0shelf.posY,
                width: storedContainerWidth,
                currentWidth: rectangle.width,
                currentHeight: rectangle.height,
                rectangles: []
            }
            rectangle.posX = newShelf.posX
            rectangle.posY = newShelf.posY
            newShelf.rectangles.push(rectangle)
            rectangle.debug = id
            ++id
            h0shelf.posY += newShelf.currentHeight
            shelves.push(newShelf)
        } else {
            newRectangles.push(rectangle)
        }
    })
    return id
}

/*
 * Step 2
 * */
function step2_safeHighRectangleContainerSpace(
    rectangles: RectangleType[],
    h0shelf: AdvancedShelfType,
    shelves: AdvancedShelfType[],
    rectangleID: number,
    step1ShelvesID: number
): number {
    let startingRecPushedLR: boolean = true
    let rectangleFitRL: boolean = true
    let rectangleMarryed: MarryRectangle = {
        leftRightRecs: [],
        rightLeftRecs: []
    }
    let rectangleMarryedLastElem: number = 0
    let rlPushWidth: number = storedContainerWidth
    let rlPushHeight: number = h0shelf.posY + h0shelf.currentHeight
    let rlPushStart: boolean = false
    let h0LRPushSize: number = 0
    let halfReached: boolean = false

    let id: number = 0
    for (; id < rectangles.length; ++id) {
        startingRecPushedLR = fitRectangleIntoShelf_LR(h0shelf, rectangles[id])
        if (startingRecPushedLR) {
            rectangles[id].debug = rectangleID
            ++rectangleID
            ++h0LRPushSize
        }
        if (!startingRecPushedLR) {
            if (rectangleFitRL) {
                if (!rlPushStart) {
                    rectangles[id].posY = rlPushHeight
                    rlPushHeight = rlPushHeight + rectangles[id].height
                    rlPushStart = true
                } else {
                    rectangles[id].posY = rlPushHeight - rectangles[id].height
                }
                rectangleFitRL = doesRectangleFitIntoShelf_RL_half(rectangles[id], rlPushWidth)
                rlPushWidth -= rectangles[id].width
                rectangles[id].posX = rlPushWidth
                rectangles[id].debug = rectangleID
                ++rectangleID

                step2_calculateMarryedRectangles(rectangles, h0shelf, rectangleMarryed, id)

                if (!rectangleFitRL) {
                    halfReached = true
                    rectangleMarryedLastElem = rectangleMarryed.leftRightRecs.length - 1
                    //Reached Containerhalf
                    let halfMatchRectangleLR: RectangleType = rectangleMarryed.leftRightRecs[rectangleMarryedLastElem]
                    let halfMatchRectangleRL: RectangleType = rectangleMarryed.rightLeftRecs[rectangleMarryed.rightLeftRecs.length - 1]
                    //Move Rectangle reached Containerhalf down
                    let rectanglePosYDiff: number = halfMatchRectangleRL.posY! - halfMatchRectangleLR.posY! - halfMatchRectangleLR.height
                    //Check if rightsided Rectangles collides
                    let rectangleCollidesHeightArr: number[] = []
                    for (let i = rectangleMarryedLastElem - 1; i >= 0; --i) {
                        let rectangleRightLeftPosY = rectangleMarryed.rightLeftRecs[i].posY! - rectanglePosYDiff
                        let rectangleLeftRightPosY = rectangleMarryed.leftRightRecs[i].posY! + rectangleMarryed.leftRightRecs[i].height
                        let resultCollision = rectangleRightLeftPosY - rectangleLeftRightPosY
                        /* Der nach unten überstehende hat höhere Priorität als der obendrüber abstehende
                            - 2. der kleinste abstehende Wert ist mehr Wert als größere abstehende Wert
                         */
                        rectangleCollidesHeightArr.push(resultCollision)
                    }
                    let rectangleCollidesHeight: number = rectangleCollidesHeightArr[0]
                    for (let i = 1; i < rectangleCollidesHeightArr.length; ++i) {
                        if (rectangleCollidesHeight > rectangleCollidesHeightArr[i]) {
                            rectangleCollidesHeight = rectangleCollidesHeightArr[i]
                        }
                    }
                    //Move colliding Rectangles up and update posX of Rectangles
                    for (let i = 0; i < rectangleMarryedLastElem; ++i) {
                        rectangleMarryed.rightLeftRecs[i].posY! -= rectanglePosYDiff + rectangleCollidesHeight
                    }

                    //Check if previous Containerhalf colliding Rectangle is deeper as the marryed Rectangle from Last Containerhalf pair
                    let heightRightSide =
                        rectangleMarryed.rightLeftRecs[rectangleMarryedLastElem - 1].posY! + rectangleMarryed.rightLeftRecs[rectangleMarryedLastElem - 1].height
                    let heightLeftSide =
                        rectangleMarryed.leftRightRecs[rectangleMarryedLastElem].posY! + rectangleMarryed.leftRightRecs[rectangleMarryedLastElem].height
                    if (heightRightSide > heightLeftSide) {
                        //Hold calculated position
                        if (rectangleCollidesHeight >= 0) {
                            rectangles[id].posY! -= rectanglePosYDiff
                        } else {
                            rectangles[id].posY! -= rectanglePosYDiff + rectangleCollidesHeight
                        }
                        let rectangleOnHalf: RectangleType = rectangleMarryed.rightLeftRecs[rectangleMarryedLastElem]
                        //new Shelf
                        const currentShelf: AdvancedShelfType = {
                            posX: rectangleOnHalf.posX!,
                            posY: rectangleOnHalf.posY! + rectangleOnHalf.height,
                            width: storedContainerWidth - rectangleOnHalf.posX!,
                            currentWidth: storedContainerWidth,
                            currentHeight: 0,
                            rectangles: []
                        }
                        const placeholder: RectangleType = {
                            width: 0,
                            height: 0
                        }
                        currentShelf.rectangles.push(placeholder)
                        shelves.push(currentShelf)
                    } else if (heightRightSide < heightLeftSide) {
                        //Move Up and Right
                        let tmpPOSX = 0
                        let reverseH0shelfRectangles: RectangleType[] = []
                        //Berechne Abstand auf X Achse nach links bis ein kleineres Rechteck beginnt
                        for (let i = 0; h0shelf.rectangles[i] != rectangleMarryed.leftRightRecs[rectangleMarryedLastElem - 1]; ++i) {
                            rectangles[id].posY = h0shelf.rectangles[i].posY! + h0shelf.rectangles[i].height
                            if (h0shelf.rectangles[i].posY! + h0shelf.rectangles[i].height < heightRightSide) {
                                break
                            } else if (heightRightSide == rectangles[id].posY!) {
                                break
                            }
                            tmpPOSX += h0shelf.rectangles[i].width
                        }
                        if (rectangles[id].posX! + tmpPOSX > storedContainerWidth) {
                            tmpPOSX = rlPushWidth
                            heightRightSide = rlPushHeight - rectangles[id].height - rectanglePosYDiff
                        }
                        //new Shelf
                        const currentShelf: AdvancedShelfType = {
                            posX: tmpPOSX,
                            posY: heightRightSide,
                            width: storedContainerWidth - tmpPOSX,
                            currentWidth: rectangles[id].width,
                            currentHeight: rectangles[id].height,
                            rectangles: []
                        }
                        rectangles[id].posY = currentShelf.posY
                        rectangles[id].posX = currentShelf.posX
                        currentShelf.rectangles.push(rectangles[id])
                        shelves.push(currentShelf)
                    } else {
                        //Move Left
                        rectangles[id].posY! -= rectanglePosYDiff
                        let tmpPOSX = 0
                        for (let i = 0; rectangles[i] != rectangleMarryed.leftRightRecs[rectangleMarryedLastElem]; ++i) {
                            if (rectangles[i].posY! + rectangles[i].height >= rectangles[id].posY! + rectangles[id].height) {
                                tmpPOSX = rectangles[i].posX! + rectangles[i].width
                            }
                        }
                        //new Shelf
                        const currentShelf: AdvancedShelfType = {
                            posX: tmpPOSX!,
                            posY: rectangleMarryed.rightLeftRecs[rectangleMarryedLastElem].posY!,
                            width: storedContainerWidth - tmpPOSX,
                            currentWidth: rectangleMarryed.rightLeftRecs[rectangleMarryedLastElem].width,
                            currentHeight: rectangleMarryed.rightLeftRecs[rectangleMarryedLastElem].height,
                            rectangles: []
                        }
                        rectangles[id].posX = currentShelf.posX
                        currentShelf.rectangles.push(rectangles[id])
                        shelves.push(currentShelf)
                    }
                    break
                }
            }
        }
    }
    ++id
    //Out of rectangles before half
    if (!halfReached && id >= rectangles.length && rectangleMarryed.rightLeftRecs.length > 0) {
        rectangleMarryedLastElem = rectangleMarryed.leftRightRecs.length - 1
        let halfMatchRectangleLR: RectangleType = rectangleMarryed.leftRightRecs[rectangleMarryedLastElem]
        let halfMatchRectangleRL: RectangleType = rectangleMarryed.rightLeftRecs[rectangleMarryed.rightLeftRecs.length - 1]
        //Move Rectangle reached Containerhalf down
        let rectanglePosYDiff: number = halfMatchRectangleRL.posY! - halfMatchRectangleLR.posY! - halfMatchRectangleLR.height
        //Check if rightsided Rectangles collides
        // let rectangleCollidesHeight: number = rlPushHeight
        let rectangleCollidesHeightArr: number[] = []
        for (let i = rectangleMarryedLastElem; i >= 0; --i) {
            let rectangleRightLeftPosY = rectangleMarryed.rightLeftRecs[i].posY! - rectanglePosYDiff
            let rectangleLeftRightPosY = rectangleMarryed.leftRightRecs[i].posY! + rectangleMarryed.leftRightRecs[i].height
            let resultCollision = rectangleRightLeftPosY - rectangleLeftRightPosY
            /* Der nach unten überstehende hat höhere Priorität als der obendrüber abstehende
                - 2. der kleinste abstehende Wert ist mehr Wert als größere abstehende Wert
             */
            rectangleCollidesHeightArr.push(resultCollision)
        }
        let rectangleCollidesHeight: number = rectangleCollidesHeightArr[0]
        for (let i = 1; i < rectangleCollidesHeightArr.length; ++i) {
            if (rectangleCollidesHeight > rectangleCollidesHeightArr[i]) {
                rectangleCollidesHeight = rectangleCollidesHeightArr[i]
            }
        }
        //Move colliding Rectangles up and update posX of Rectangles
        for (let i = 0; i <= rectangleMarryedLastElem; ++i) {
            rectangleMarryed.rightLeftRecs[i].posY! -= rectanglePosYDiff + rectangleCollidesHeight
        }
    }

    for (; id < rectangles.length; ++id) {
        h0LRPushSize = h0shelf.rectangles.length - 1
        //Füge aktuelles Rectangle mit Next Fit hinzu, in das h0shelf
        let rectangleFits: boolean = false
        for (let i = step1ShelvesID - 1; i < shelves.length; ++i) {
            rectangleFits = doesRectangleFitIntoShelf_LR(shelves[i], rectangles[id])
            if (rectangleFits) {
                rectangles[id].posX = shelves[i].posX + shelves[i].currentWidth
                rectangles[id].posY = shelves[i].posY
                shelves[i].rectangles.push(rectangles[id])
                shelves[i].currentWidth += rectangles[id].width

                rectangles[id].debug = rectangleID
                ++rectangleID
                break
            }
        }
        if (!rectangleFits) {
            let quit: boolean = false
            //Wenn die Rectanglehöhe im rechten Bereich noch nicht die Höhe von H0 überschreitet
            for (let i = h0LRPushSize; i >= 0; --i) {
                let heightLeft = h0shelf.posY! + h0shelf.rectangles[i].height
                let heightRight = shelves[shelves.length - 1].posY + shelves[shelves.length - 1].currentHeight
                if (i > 0) {
                    if (heightLeft > heightRight) {
                        let tmpPOSX = 0
                        let tmpWidth = 0
                        tmpWidth = storedContainerWidth - h0shelf.rectangles[i].posX! - h0shelf.rectangles[i].width
                        tmpPOSX = h0shelf.rectangles[i].posX! + h0shelf.rectangles[i].width
                        //Luft nach oben vorhanden
                        //HX
                        const newShelf: AdvancedShelfType = {
                            posX: tmpPOSX,
                            posY: shelves[shelves.length - 1].posY + shelves[shelves.length - 1].currentHeight,
                            width: tmpWidth,
                            currentWidth: 0,
                            currentHeight: rectangles[id].height,
                            rectangles: []
                        }
                        shelves.push(newShelf)
                        pushRectangleInAdvancedShelf_LR(newShelf, rectangles[id])
                        rectangles[id].debug = rectangleID
                        ++rectangleID
                        --h0LRPushSize
                        break
                    } else if (heightLeft <= heightRight) {
                        if (i - 1 == 0) {
                            if (h0shelf.posY + h0shelf.rectangles[0].height <= heightLeft) {
                                h0shelf.currentHeight = shelves[shelves.length - 1].posY + shelves[shelves.length - 1].currentHeight - h0shelf.posY
                                //Luft nach oben nicht vorhanden & ende h0.height erreicht
                                //HX
                                const newShelf: AdvancedShelfType = {
                                    posX: 0,
                                    posY: h0shelf.posY + h0shelf.currentHeight,
                                    width: storedContainerWidth,
                                    currentWidth: 0,
                                    currentHeight: rectangles[id].height,
                                    rectangles: []
                                }
                                pushRectangleInAdvancedShelf_LR(newShelf, rectangles[id])
                                rectangles[id].debug = rectangleID
                                ++rectangleID
                                shelves.push(newShelf)
                                quit = true
                                break
                            } else if (
                                h0shelf.posY + h0shelf.rectangles[0].height >
                                rectangleMarryed.rightLeftRecs[rectangleMarryedLastElem].posY! + rectangleMarryed.rightLeftRecs[rectangleMarryedLastElem].height
                            ) {
                                //Luft nach oben nicht vorhanden & ende h0.height erreicht
                                //HX
                                const newShelf: AdvancedShelfType = {
                                    posX: h0shelf.rectangles[0].width,
                                    posY: shelves[shelves.length - 1].posY + shelves[shelves.length - 1].currentHeight,
                                    width: storedContainerWidth - h0shelf.rectangles[0].width,
                                    currentWidth: 0,
                                    currentHeight: rectangles[id].height,
                                    rectangles: []
                                }
                                pushRectangleInAdvancedShelf_LR(newShelf, rectangles[id])
                                rectangles[id].debug = rectangleID
                                ++rectangleID
                                shelves.push(newShelf)
                                quit = true
                                break
                            }
                        }
                    }
                } else {
                    h0shelf.currentHeight = shelves[shelves.length - 1].posY + shelves[shelves.length - 1].currentHeight - h0shelf.posY
                    //Luft nach oben vorhanden
                    //HX
                    const newShelf: AdvancedShelfType = {
                        posX: 0,
                        posY: h0shelf.posY + h0shelf.currentHeight,
                        width: storedContainerWidth,
                        currentWidth: 0,
                        currentHeight: rectangles[id].height,
                        rectangles: []
                    }
                    shelves.push(newShelf)
                    pushRectangleInAdvancedShelf_LR(newShelf, rectangles[id])
                    rectangles[id].debug = rectangleID
                    ++rectangleID
                    quit = true
                    break
                }
            }
            if (quit) {
                ++id
                break
            }
        }
    }
    return id
}

function step2_calculateMarryedRectangles(rectangles: RectangleType[], h0shelf: AdvancedShelfType, rectangleMarryed: MarryRectangle, constID: number) {
    let rectangleMatchFound: boolean = false
    h0shelf.rectangles.every(currentRectangleInShelf => {
        if (currentRectangleInShelf.posX! + currentRectangleInShelf.width > rectangles[constID].posX!) {
            rectangleMarryed.leftRightRecs?.push(currentRectangleInShelf)
            rectangleMarryed.rightLeftRecs?.push(rectangles[constID])
            rectangleMatchFound = true
            return false
        }
        return true
    })
    if (!rectangleMatchFound) {
        console.debug("WARNING/ERROR: Found no rectangle match!")
    }
    if (rectangleMarryed.leftRightRecs.length - 1 != rectangleMarryed.rightLeftRecs.length - 1) {
        console.debug("WARNING/ERROR: RectangleMarryed Containersize don't match!")
    }
}

/*
 * Step 3
 * */
function step3_ffdh(rectangles: RectangleType[], shelves: AdvancedShelfType[], nextRectangleID: number, rectangleID: number, step1ShelvesID: number): number {
    //Füge aktuelles Rectangle mit Next Fit hinzu, in das h0shelf
    //Missing check if shelf is full and next rectangle doesnt fit (cause of width)
    for (; nextRectangleID < rectangles.length; ++nextRectangleID) {
        let success: boolean = false
        for (let i = step1ShelvesID - 1; i < shelves.length; ++i) {
            success = fitRectangleIntoAdvancedShelf_LR(shelves[i], rectangles[nextRectangleID])
            if (success) {
                break
            }
        }
        if (!success) {
            let lastShelve = shelves[shelves.length - 1]
            let newShelfPosY = lastShelve.posY + lastShelve.currentHeight
            const newShelf: AdvancedShelfType = {
                posX: 0,
                posY: newShelfPosY,
                width: storedContainerWidth,
                currentWidth: 0,
                currentHeight: rectangles[nextRectangleID].height,
                rectangles: []
            }
            rectangles[nextRectangleID].debug = rectangleID
            ++rectangleID
            shelves.push(newShelf)
            pushRectangleInShelf_LR(newShelf, rectangles[nextRectangleID])
        } else {
            rectangles[nextRectangleID].debug = rectangleID
            ++rectangleID
        }
    }
    return nextRectangleID
}

/*
 * Left to Right
 * */
function fitRectangleIntoShelf_LR(shelf: ShelfType, rectangle: RectangleType): boolean {
    if (doesRectangleFitIntoShelf_LR(shelf, rectangle)) {
        pushRectangleInShelf_LR(shelf, rectangle)
        return true
    } else {
        return false
    }
}

const doesRectangleFitIntoShelf_LR = (shelf: ShelfType, rectangle: RectangleType) => {
    return shelf.rectangles.length === 0 || rectangle.width + shelf.currentWidth <= shelf.width
}

const pushRectangleInShelf_LR = (shelf: ShelfType, rectangle: RectangleType) => {
    rectangle.posX = shelf.currentWidth
    rectangle.posY = shelf.posY
    shelf.rectangles.push(rectangle)
    shelf.currentWidth += rectangle.width
}

function fitRectangleIntoAdvancedShelf_LR(shelf: AdvancedShelfType, rectangle: RectangleType): boolean {
    if (doesRectangleFitIntoAdvancedShelf_LR(shelf, rectangle)) {
        pushRectangleInAdvancedShelf_LR(shelf, rectangle)
        return true
    } else {
        return false
    }
}

const doesRectangleFitIntoAdvancedShelf_LR = (shelf: ShelfType, rectangle: RectangleType) => {
    return shelf.rectangles.length === 0 || rectangle.width + shelf.currentWidth <= shelf.width
}

const pushRectangleInAdvancedShelf_LR = (shelf: AdvancedShelfType, rectangle: RectangleType) => {
    rectangle.posX = shelf.posX + shelf.currentWidth
    rectangle.posY = shelf.posY
    shelf.rectangles.push(rectangle)
    shelf.currentWidth += rectangle.width
}

/*
 * Right to Left
 * */
const doesRectangleFitIntoShelf_RL_half = (rectangle: RectangleType, rlPushWidth: number) => {
    return rlPushWidth - rectangle.width >= storedContainerWidthHalf
}
