import Steinberg from "./Steinberg"
import { RectangleType } from "../Algorithm.types"
import { sortDescendingWidth } from "../Algorithm.functions"

describe("test Steinberg", () => {
    test("run", () => {
        const rectangles: RectangleType[] = [
            {
                id: 0,
                width: 1,
                height: 20,
                posX: 0,
                posY: 0
            },
            {
                id: 1,
                width: 2,
                height: 20,
                posX: 1,
                posY: 0
            },
            {
                id: 2,
                width: 1,
                height: 30,
                posX: 0,
                posY: 20
            },
            {
                id: 3,
                width: 2,
                height: 30,
                posX: 1,
                posY: 20
            },
            {
                id: 4,
                width: 9,
                height: 50,
                posX: 3,
                posY: 0
            },
            {
                id: 5,
                width: 7,
                height: 50,
                posX: 12,
                posY: 0
            },
            {
                id: 6,
                width: 2,
                height: 50,
                posX: 19,
                posY: 0
            },
            {
                id: 7,
                width: 3,
                height: 50,
                posX: 21,
                posY: 0
            },
            {
                id: 8,
                width: 12,
                height: 17,
                posX: 24,
                posY: 0
            },
            {
                id: 9,
                width: 12,
                height: 9,
                posX: 24,
                posY: 17
            },
            {
                id: 10,
                width: 3,
                height: 26,
                posX: 36,
                posY: 0
            },
            {
                id: 11,
                width: 1,
                height: 26,
                posX: 39,
                posY: 0
            },
            {
                id: 12,
                width: 13,
                height: 21,
                posX: 24,
                posY: 26
            },
            {
                id: 13,
                width: 3,
                height: 21,
                posX: 37,
                posY: 26
            },
            {
                id: 14,
                width: 16,
                height: 2,
                posX: 24,
                posY: 47
            },
            {
                id: 15,
                width: 16,
                height: 1,
                posX: 24,
                posY: 49
            },
            {
                id: 16,
                width: 1,
                height: 19,
                posX: 40,
                posY: 0
            },
            {
                id: 17,
                width: 1,
                height: 25,
                posX: 40,
                posY: 19
            },
            {
                id: 18,
                width: 1,
                height: 3,
                posX: 40,
                posY: 44
            },
            {
                id: 19,
                width: 1,
                height: 3,
                posX: 40,
                posY: 47
            },
            {
                id: 20,
                width: 1,
                height: 50,
                posX: 41,
                posY: 0
            },
            {
                id: 21,
                width: 1,
                height: 50,
                posX: 42,
                posY: 0
            },
            {
                id: 22,
                width: 1,
                height: 47,
                posX: 43,
                posY: 0
            },
            {
                id: 23,
                width: 1,
                height: 3,
                posX: 43,
                posY: 47
            },
            {
                id: 24,
                width: 3,
                height: 10,
                posX: 44,
                posY: 0
            },
            {
                id: 25,
                width: 2,
                height: 10,
                posX: 47,
                posY: 0
            },
            {
                id: 26,
                width: 1,
                height: 3,
                posX: 49,
                posY: 0
            },
            {
                id: 27,
                width: 1,
                height: 7,
                posX: 49,
                posY: 3
            },
            {
                id: 28,
                width: 2,
                height: 40,
                posX: 44,
                posY: 10
            },
            {
                id: 29,
                width: 1,
                height: 40,
                posX: 46,
                posY: 10
            },
            {
                id: 30,
                width: 3,
                height: 12,
                posX: 47,
                posY: 10
            },
            {
                id: 31,
                width: 3,
                height: 28,
                posX: 47,
                posY: 22
            }
        ]

        const tmp: RectangleType[] = []
        rectangles.forEach(r => {
            tmp.push({
                ...r,
                posX: undefined,
                posY: undefined
            })
        })

        let str = ""
        tmp.forEach(t => {
            str += "[" + t.width + ", " + t.height + "],\n"
        })

        console.log(str)

        const result = Steinberg(50, 100, tmp, false, false)

        console.log(result)
    })
})
