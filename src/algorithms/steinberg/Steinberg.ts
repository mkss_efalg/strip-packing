import { RectangleType } from "../Algorithm.types"
import { deepCopy, getMaxHeight, getMaxWidth, getSumArea, sortDescendingHeight, sortDescendingWidth } from "../Algorithm.functions"

let cSequence: boolean[] = []

const p1Marker = "P1"
const p1InverseMarker = "P-1"
const p3Marker = "P3"
const p3InverseMarker = "P-3"
const p2Marker = "P2"
const p2InverseMarker = "P-2"
const p0Marker = "P0"

let startTime
let endTime

export default function Steinberg(
    stripWidth: number,
    stripHeight: number | undefined,
    rectangles: RectangleType[],
    steinbergHeuristic: boolean,
    enableSteinbergInverseFirst: boolean
) {
    if (rectangles.length == 0) {
        return {
            sortedRectangles: [],
            markers: [],
            error: "No input rectangles present.",
            runTime: undefined
        }
    }
    const rectanglesCopy = deepCopy(rectangles)

    const sumArea = getSumArea(rectanglesCopy)
    const maxWidth = getMaxWidth(rectanglesCopy)
    const maxHeight = getMaxHeight(rectanglesCopy)

    const steinbergHeight = stripHeight ?? estimatedSteinbergHeight(stripWidth, maxWidth, maxHeight, sumArea)

    if (
        maxWidth > stripWidth ||
        maxHeight > steinbergHeight ||
        2 * sumArea > stripWidth * steinbergHeight - Math.max(2 * maxWidth - stripWidth, 0) * Math.max(2 * maxHeight - steinbergHeight, 0)
    ) {
        return {
            sortedRectangles: [],
            markers: [],
            error: "Rectangles do not fit into the strip container. \n Please check if Steinberg prerequisites are met.",
            runTime: undefined
        }
    }

    let packing: RectangleType[] = []
    startTime = performance.now()
    cSequence = !enableSteinbergInverseFirst ? [false, true] : [true, false]
    packing = steinberg(0, 0, stripWidth, steinbergHeight, rectanglesCopy, packing)
    const sortedRectangles = steinbergHeuristic ? dropPackingHeuristic(packing) : packing
    endTime = performance.now()
    return {
        sortedRectangles,
        markers: [],
        error: "",
        runTime: endTime - startTime
    }
}

function estimatedSteinbergHeight(stripWidth: number, maxWidth: number, maxHeight: number, sumArea: number) {
    return 2 * maxWidth >= stripWidth && sumArea <= maxHeight * stripWidth
        ? (sumArea + 4 * maxWidth * maxHeight - maxHeight * stripWidth) / (2 * maxWidth)
        : (2 * sumArea) / stripWidth
}

function dropPackingHeuristic(packing: RectangleType[]) {
    packing.sort(sortDescendingHeight)
    let isFalling = true

    while (isFalling) {
        isFalling = false
        for (let i = 0; i < packing.length; i++) {
            let maxY = 0
            for (let j = 0; j < packing.length; j++) {
                if (
                    i != j &&
                    Math.min(packing[i].posX! + packing[i].width, packing[j].posX! + packing[j].width) - Math.max(packing[i].posX!, packing[j].posX!) > 0 &&
                    packing[j].posY! + packing[j].height <= packing[i].posY!
                ) {
                    maxY = packing[j].posY! + packing[j].height > maxY ? packing[j].posY! + packing[j].height : maxY
                }
            }
            if (maxY != packing[i].posY) {
                packing[i].posY = maxY
                isFalling = true
            }
        }
    }

    return packing
}

function steinberg(
    posX: number,
    posY: number,
    stripWidth: number,
    stripHeight: number,
    rectangles: RectangleType[],
    packing: RectangleType[]
): RectangleType[] {
    if (rectangles.length == 0) {
        return packing
    }

    const c1Var = c1(posX, posY, stripWidth, stripHeight, rectangles, packing)
    if (c1Var) {
        return c1Var
    }

    const sumArea = getSumArea(rectangles)

    if (rectangles.length > 1) {
        const c3Var = c3(sumArea, posX, posY, stripWidth, stripHeight, rectangles, packing)
        if (c3Var) {
            return c3Var
        }

        const c2Var = c2(sumArea, posX, posY, stripWidth, stripHeight, rectangles, packing)
        if (c2Var) {
            return c2Var
        }
    }

    const c0Var = c0(sumArea, posX, posY, stripWidth, stripHeight, rectangles, packing)
    if (c0Var) {
        return c0Var
    }

    return packing
}

function c1(posX: number, posY: number, stripWidth: number, stripHeight: number, rectangles: RectangleType[], packing: RectangleType[]) {
    for (const sequence of cSequence) {
        rectangles.sort(!sequence ? sortDescendingWidth : sortDescendingHeight)
        if (!sequence ? rectangles[0].width >= stripWidth / 2 : rectangles[0].height >= stripHeight / 2) {
            console.debug(!sequence ? p1Marker : p1InverseMarker)
            return p1(posX, posY, stripWidth, stripHeight, rectangles, packing, sequence)
        }
    }
}

function c3(sumArea: number, posX: number, posY: number, stripWidth: number, stripHeight: number, rectangles: RectangleType[], packing: RectangleType[]) {
    for (const sequence of cSequence) {
        rectangles.sort(!sequence ? sortDescendingWidth : sortDescendingHeight)

        let p3Enable = false
        let currentSumArea = 0
        let i = 0
        while (i < rectangles.length - 1 && !p3Enable) {
            currentSumArea += rectangles[i].width * rectangles[i].height
            p3Enable =
                sumArea - (stripWidth * stripHeight) / 4 <= currentSumArea &&
                currentSumArea <= (3 * stripWidth * stripHeight) / 8 &&
                (!sequence ? rectangles[i + 1].width <= stripWidth / 4 : rectangles[i + 1].height <= stripHeight / 4)
            i++
        }
        if (p3Enable) {
            console.debug(!sequence ? p3Marker : p3InverseMarker)
            return p3(i - 1, currentSumArea, posX, posY, stripWidth, stripHeight, rectangles, packing, sequence)
        }
    }
}

function c2(sumArea: number, posX: number, posY: number, stripWidth: number, stripHeight: number, rectangles: RectangleType[], packing: RectangleType[]) {
    for (const sequence of cSequence) {
        let p2Enable = false
        let i = 0
        let k = 0
        while (i < rectangles.length && !p2Enable) {
            k = 0
            while (k < i && !p2Enable) {
                p2Enable =
                    rectangles[i].width >= stripWidth / 4 &&
                    rectangles[k].width >= stripWidth / 4 &&
                    rectangles[i].height >= stripHeight / 4 &&
                    rectangles[k].height >= stripHeight / 4 &&
                    2 * (sumArea - rectangles[i].width * rectangles[i].height - rectangles[k].width * rectangles[k].height) <=
                        (!sequence
                            ? (stripWidth - Math.max(rectangles[i].width, rectangles[k].width)) * stripHeight
                            : (stripHeight - Math.max(rectangles[i].height, rectangles[k].height)) * stripWidth)
                k++
            }
            i++
        }
        if (p2Enable) {
            console.debug(!sequence ? p2Marker : p2InverseMarker)
            return p2(i - 1, k - 1, posX, posY, stripWidth, stripHeight, rectangles, packing, sequence)
        }
    }
}

function c0(sumArea: number, posX: number, posY: number, stripWidth: number, stripHeight: number, rectangles: RectangleType[], packing: RectangleType[]) {
    let p0Enable = false
    let i = 0
    while (i < rectangles.length && !p0Enable) {
        p0Enable = sumArea - (stripWidth * stripHeight) / 4 <= rectangles[i].width * rectangles[i].height
        i++
    }
    if (p0Enable) {
        console.debug(p0Marker)
        return p0(i - 1, posX, posY, stripWidth, stripHeight, rectangles, packing)
    }
}

function p1(
    posX: number,
    posY: number,
    stripWidth: number,
    stripHeight: number,
    rectangles: RectangleType[],
    packing: RectangleType[],
    inverse: boolean
): RectangleType[] {
    let packingLast = packing.length - 1

    packing.push({
        ...rectangles[0],
        posX: posX,
        posY: posY,
        steinbergMarker: !inverse ? p1Marker : p1InverseMarker
    })

    let sumHeightOrWidth = !inverse ? rectangles[0].height : rectangles[0].width
    let i = 1
    while (i < rectangles.length && (!inverse ? rectangles[i].width >= stripWidth / 2 : rectangles[i].height >= stripHeight / 2)) {
        packing.push({
            ...rectangles[i],
            posX: !inverse ? posX : packing[packingLast + i].posX! + packing[packingLast + i].width,
            posY: !inverse ? packing[packingLast + i].posY! + packing[packingLast + i].height : posY,
            steinbergMarker: !inverse ? p1Marker : p1InverseMarker
        })
        sumHeightOrWidth += !inverse ? rectangles[i].height : rectangles[i].width
        i++
    }

    rectangles.splice(0, i)

    if (rectangles.length == 0) {
        return packing
    }

    rectangles.sort(!inverse ? sortDescendingHeight : sortDescendingWidth)

    if (!inverse ? rectangles[0].height <= stripHeight - sumHeightOrWidth : rectangles[0].width <= stripWidth - sumHeightOrWidth) {
        return steinberg(
            !inverse ? posX : posX + sumHeightOrWidth,
            !inverse ? posY + sumHeightOrWidth : posY,
            !inverse ? stripWidth : stripWidth - sumHeightOrWidth,
            !inverse ? stripHeight - sumHeightOrWidth : stripHeight,
            rectangles,
            packing
        )
    } else {
        packingLast = packing.length - 1

        packing.push({
            ...rectangles[0],
            posX: posX + stripWidth - rectangles[0].width,
            posY: posY + stripHeight - rectangles[0].height,
            steinbergMarker: !inverse ? p1Marker : p1InverseMarker
        })

        let sumWidthOrHeight = !inverse ? rectangles[0].width : rectangles[0].height
        i = 1
        while (
            i < rectangles.length &&
            (!inverse ? rectangles[i].height > stripHeight - sumHeightOrWidth : rectangles[i].width > stripWidth - sumHeightOrWidth)
        ) {
            packing.push({
                ...rectangles[i],
                posX: !inverse ? packing[packingLast + i].posX! - rectangles[i].width : posX + stripWidth - rectangles[i].width,
                posY: !inverse ? posY + stripHeight - rectangles[i].height : packing[packingLast + i].posY! - rectangles[i].height,
                steinbergMarker: !inverse ? p1Marker : p1InverseMarker
            })
            sumWidthOrHeight += !inverse ? rectangles[i].width : rectangles[i].height
            i++
        }

        rectangles.splice(0, i)

        return steinberg(
            !inverse ? posX : posX + sumWidthOrHeight,
            !inverse ? posY + sumHeightOrWidth : posY,
            stripWidth - sumWidthOrHeight,
            stripHeight - sumHeightOrWidth,
            rectangles,
            packing
        )
    }
}

function p3(
    currentIndex: number,
    currentSumArea: number,
    posX: number,
    posY: number,
    stripWidth: number,
    stripHeight: number,
    rectangles: RectangleType[],
    packing: RectangleType[],
    inverse: boolean
): RectangleType[] {
    const widthOrHeight1 = !inverse
        ? Math.max(stripWidth / 2, (2 * currentSumArea) / stripHeight)
        : Math.max(stripHeight / 2, (2 * currentSumArea) / stripWidth)
    const widthOrHeight2 = (!inverse ? stripWidth : stripHeight) - widthOrHeight1

    const rectangles1: RectangleType[] = []
    const rectangles2: RectangleType[] = []

    for (let i = 0; i < rectangles.length; i++) {
        if (i <= currentIndex) {
            rectangles1.push(rectangles[i])
        } else {
            rectangles2.push(rectangles[i])
        }
    }

    packing = steinberg(posX, posY, !inverse ? widthOrHeight1 : stripWidth, !inverse ? stripHeight : widthOrHeight1, rectangles1, packing)
    return steinberg(
        !inverse ? posX + widthOrHeight1 : posX,
        !inverse ? posY : posY + widthOrHeight1,
        !inverse ? widthOrHeight2 : stripWidth,
        !inverse ? stripHeight : widthOrHeight2,
        rectangles2,
        packing
    )
}

function p2(
    index1: number,
    index2: number,
    posX: number,
    posY: number,
    stripWidth: number,
    stripHeight: number,
    rectangles: RectangleType[],
    packing: RectangleType[],
    inverse: boolean
): RectangleType[] {
    if (!inverse ? rectangles[index2].width > rectangles[index1].width : rectangles[index2].height > rectangles[index1].height) {
        const indexTmp = index1
        index1 = index2
        index2 = indexTmp
    }

    packing.push({
        ...rectangles[index1],
        posX: posX,
        posY: posY,
        steinbergMarker: !inverse ? p2Marker : p2InverseMarker
    })
    packing.push({
        ...rectangles[index2],
        posX: !inverse ? posX : posX + rectangles[index1].width,
        posY: !inverse ? posY + rectangles[index1].height : posY,
        steinbergMarker: !inverse ? p2Marker : p2InverseMarker
    })

    const rectangleWidthOrHeight = !inverse ? rectangles[index1].width : rectangles[index1].height
    if (index1 < index2) {
        rectangles.splice(index2, 1)
        rectangles.splice(index1, 1)
    } else {
        rectangles.splice(index1, 1)
        rectangles.splice(index2, 1)
    }

    return steinberg(
        !inverse ? posX + rectangleWidthOrHeight : posX,
        !inverse ? posY : posY + rectangleWidthOrHeight,
        !inverse ? stripWidth - rectangleWidthOrHeight : stripWidth,
        !inverse ? stripHeight : stripHeight - rectangleWidthOrHeight,
        rectangles,
        packing
    )
}

function p0(
    index: number,
    posX: number,
    posY: number,
    stripWidth: number,
    stripHeight: number,
    rectangles: RectangleType[],
    packing: RectangleType[]
): RectangleType[] {
    packing.push({
        ...rectangles[index],
        posX: posX,
        posY: posY,
        steinbergMarker: p0Marker
    })

    const rectangleWidth = rectangles[index].width
    rectangles.splice(index, 1)

    return steinberg(posX + rectangleWidth, posY, stripWidth - rectangleWidth, stripHeight, rectangles, packing)
}
