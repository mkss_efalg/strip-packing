export interface RectangleType {
    id?: number
    posX?: number
    posY?: number
    width: number
    height: number
    steinbergMarker?: string
    debug?: number
}

export interface ShelfType {
    posY: number
    width: number
    currentWidth: number
    currentHeight: number
    rectangles: RectangleType[]
}

export interface AdvancedShelfType {
    posX: number
    posY: number
    width: number
    currentWidth: number
    currentHeight: number
    rectangles: RectangleType[]
}

export interface MarryRectangle {
    leftRightRecs: RectangleType[]
    rightLeftRecs: RectangleType[]
}

export interface HorizontalSplitGroup {
    groupID: number
    rectangles: RectangleType[]
    width?: number
}

export interface HorizontalSpGroupConfiguration {
    groupIDs: number[]
    sumWidth: number
}

export interface TableuConfiguration {
    tableu: number[][]
}

export interface HorizontalSplitGroupVisited {
    groupID: number[]
    rectangles: RectangleType[]
}

export interface RectangleWithGroupReferenceType {
    rectangle: RectangleType
    groupID: number
    groupWidth: number
}

export interface RectanglesConfiguration {
    rectangles: RectangleType[]
    // groupIDs: number[]
    width: number
    // currentWidth?: number
}
