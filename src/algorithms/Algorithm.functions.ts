import {RectangleType} from "./Algorithm.types"

export const sortDescendingHeight = (rectangleA: RectangleType, rectangleB: RectangleType) => rectangleB.height - rectangleA.height
export const sortDescendingWidth = (rectangleA: RectangleType, rectangleB: RectangleType) => rectangleB.width - rectangleA.width

export function getSumArea(rectangles: RectangleType[]) {
    let sumArea = 0
    for (let i = 0; i < rectangles.length; i++) {
        sumArea += rectangles[i].width * rectangles[i].height
    }
    return sumArea
}

export function getMaxWidth(rectangles: RectangleType[]) {
    let maxWidth = rectangles[0].width

    for (let i = 1; i < rectangles.length; i++) {
        if (rectangles[i].width > maxWidth) {
            maxWidth = rectangles[i].width
        }
    }

    return maxWidth
}

export function getMaxHeight(rectangles: RectangleType[]) {
    let maxHeight = rectangles[0].height

    for (let i = 1; i < rectangles.length; i++) {
        if (rectangles[i].height > maxHeight) {
            maxHeight = rectangles[i].height
        }
    }

    return maxHeight
}

export function deepCopy<T>(obj: T): T {
    if (obj === null || typeof obj !== "object") {
        return obj
    }

    if (Array.isArray(obj)) {
        return obj.map(item => deepCopy(item)) as T
    }

    const copiedObj: Record<string, any> = {}
    for (const key in obj) {
        if (obj.hasOwnProperty(key)) {
            copiedObj[key] = deepCopy(obj[key])
        }
    }

    return copiedObj as T
}
