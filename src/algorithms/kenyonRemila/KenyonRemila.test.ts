import KenyonRemila from "./KenyonRemila"
import { RectangleType, RectangleWithGroupReferenceType } from "../Algorithm.types"
import { sortDescendingWidth } from "../Algorithm.functions"

describe("test Kenyon", () => {
    test("run", () => {
        const rectangles: RectangleType[] = [
            {
                id: 0,
                width: 50,
                height: 3,
                posX: 0,
                posY: 0
            },
            {
                id: 1,
                width: 50,
                height: 1,
                posX: 0,
                posY: 0
            },
            {
                id: 2,
                width: 45,
                height: 2,
                posX: 0,
                posY: 0
            },
            {
                id: 3,
                width: 40,
                height: 2,
                posX: 0,
                posY: 0
            },
            {
                id: 4,
                width: 35,
                height: 3,
                posX: 0,
                posY: 0
            },
            {
                id: 5,
                width: 35,
                height: 3,
                posX: 0,
                posY: 0
            },
            {
                id: 6,
                width: 34,
                height: 4,
                posX: 0,
                posY: 0
            },
            {
                id: 7,
                width: 34,
                height: 1,
                posX: 0,
                posY: 0
            },
            {
                id: 8,
                width: 34,
                height: 1,
                posX: 0,
                posY: 0
            },
            {
                id: 9,
                width: 33.5,
                height: 3,
                posX: 0,
                posY: 0
            },
            {
                id: 10,
                width: 33,
                height: 2,
                posX: 0,
                posY: 0
            },
            {
                id: 11,
                width: 27.5,
                height: 2,
                posX: 0,
                posY: 0
            },
            {
                id: 12,
                width: 24.5,
                height: 3,
                posX: 0,
                posY: 0
            },
            {
                id: 13,
                width: 20.5,
                height: 6,
                posX: 0,
                posY: 0
            },
            {
                id: 14,
                width: 18.5,
                height: 5,
                posX: 0,
                posY: 0
            },
            {
                id: 15,
                width: 18.5,
                height: 1,
                posX: 0,
                posY: 0
            },
            {
                id: 16,
                width: 15,
                height: 3,
                posX: 0,
                posY: 0
            },
            {
                id: 17,
                width: 13,
                height: 5,
                posX: 0,
                posY: 0
            }
        ]

        const rectangles2: RectangleType[] = [
            {
                id: 0,
                width: 1,
                height: 3,
                posX: 0,
                posY: 0
            },
            {
                id: 1,
                width: 1,
                height: 8,
                posX: 0,
                posY: 3
            },
            {
                id: 2,
                width: 1,
                height: 7,
                posX: 0,
                posY: 11
            },
            {
                id: 3,
                width: 1,
                height: 1,
                posX: 0,
                posY: 18
            },
            {
                id: 4,
                width: 3,
                height: 9,
                posX: 1,
                posY: 0
            },
            {
                id: 5,
                width: 3,
                height: 10,
                posX: 1,
                posY: 9
            },
            {
                id: 6,
                width: 3,
                height: 19,
                posX: 4,
                posY: 0
            },
            {
                id: 7,
                width: 1,
                height: 19,
                posX: 7,
                posY: 0
            },
            {
                id: 8,
                width: 10,
                height: 3,
                posX: 8,
                posY: 0
            },
            {
                id: 9,
                width: 32,
                height: 3,
                posX: 18,
                posY: 0
            },
            {
                id: 10,
                width: 10,
                height: 4,
                posX: 8,
                posY: 3
            },
            {
                id: 11,
                width: 32,
                height: 4,
                posX: 18,
                posY: 3
            },
            {
                id: 12,
                width: 28,
                height: 1,
                posX: 8,
                posY: 7
            },
            {
                id: 13,
                width: 14,
                height: 1,
                posX: 36,
                posY: 7
            },
            {
                id: 14,
                width: 32,
                height: 11,
                posX: 8,
                posY: 8
            },
            {
                id: 15,
                width: 10,
                height: 11,
                posX: 40,
                posY: 8
            },
            {
                id: 16,
                width: 3,
                height: 3,
                posX: 0,
                posY: 19
            },
            {
                id: 17,
                width: 3,
                height: 1,
                posX: 0,
                posY: 22
            },
            {
                id: 18,
                width: 9,
                height: 4,
                posX: 3,
                posY: 19
            },
            {
                id: 19,
                width: 6,
                height: 4,
                posX: 12,
                posY: 19
            },
            {
                id: 20,
                width: 2,
                height: 4,
                posX: 18,
                posY: 19
            },
            {
                id: 21,
                width: 1,
                height: 4,
                posX: 20,
                posY: 19
            },
            {
                id: 22,
                width: 29,
                height: 1,
                posX: 21,
                posY: 19
            },
            {
                id: 23,
                width: 29,
                height: 3,
                posX: 21,
                posY: 20
            },
            {
                id: 24,
                width: 3,
                height: 1,
                posX: 0,
                posY: 23
            },
            {
                id: 25,
                width: 2,
                height: 1,
                posX: 3,
                posY: 23
            },
            {
                id: 26,
                width: 7,
                height: 1,
                posX: 5,
                posY: 23
            },
            {
                id: 27,
                width: 38,
                height: 1,
                posX: 12,
                posY: 23
            },
            {
                id: 28,
                width: 14,
                height: 1,
                posX: 0,
                posY: 24
            },
            {
                id: 29,
                width: 15,
                height: 1,
                posX: 14,
                posY: 24
            },
            {
                id: 30,
                width: 13,
                height: 1,
                posX: 29,
                posY: 24
            },
            {
                id: 31,
                width: 8,
                height: 1,
                posX: 42,
                posY: 24
            }
        ]

        const tmp: RectangleType[] = []
        rectangles.forEach(r => {
            tmp.push({
                ...r,
                posX: undefined,
                posY: undefined
            })
        })

        // let str = ""
        // tmp.forEach(t => {
        //     str += "[" + t.width + ", " + t.height + "],\n"
        // })
        //
        // console.log(str)
        //#############################################################################

        // {
        //     //the fuck?
        // function generateCombinationsAndPermutations(
        //     arr: number[],
        //     size: number,
        //     result: number[][] = [],
        //     current: number[] = [],
        //     start: number = 0
        // ): number[][] {
        //     if (current.length === size) {
        //         result.push([...current])
        //         return result
        //     }
        //
        //     for (let i = start; i < arr.length; i++) {
        //         current.push(arr[i])
        //         generateCombinationsAndPermutations(arr, size, result, current, i)
        //         current.pop()
        //     }
        //
        //     return result
        // }
        //
        // // Example usage:
        // const numbers = [0, 1, 2, 3]
        // const combinationsAndPermutations = generateCombinationsAndPermutations(numbers, 2)
        // console.log("Combinations and Permutations:", combinationsAndPermutations)
        // }

        // // {
        // // Working generator
        // function generateAllCombinationsWithRepetition(arr: number[], size: number, n: number, result: number[][] = [], current: number[] = []): number[][] {
        //     if (current.length === size) {
        //         result.push([...current])
        //         return result
        //     }
        //
        //     for (let i = n; i < arr.length; i++) {
        //         current.push(arr[i])
        //         generateAllCombinationsWithRepetition(arr, size, i, result, current)
        //         current.pop()
        //     }
        //
        //     return result
        // }
        //
        // function generateAllNumbersCombinationsWithRepetition(maxNumber: number, maxSize: number): number[][] {
        //     const numbers = Array.from({ length: maxNumber + 1 }, (_, i) => i)
        //     const result: number[][] = []
        //
        //     for (let i = 0; i <= maxNumber; i++) {
        //         const combinations = generateAllCombinationsWithRepetition(numbers, maxSize, i, [], [i])
        //         result.push(...combinations)
        //     }
        //
        //     return result
        // }
        //
        // // Example usage:
        // const maxNumber = 3
        // const maxSize = 3
        // const allNumbersCombinationsWithRepetition = generateAllNumbersCombinationsWithRepetition(maxNumber, maxSize)
        // console.log("All Numbers Combinations with Repetition:", allNumbersCombinationsWithRepetition)
        // console.log("solution size:", allNumbersCombinationsWithRepetition[0].length * allNumbersCombinationsWithRepetition.length)
        //
        // // }
        // let groupsTmp: number[][] = [[1], [1, 1], [0.8], [0.7], [0.7], [0.68], [0.68, 0.68], [0.66], [0.55], [0.49], [0.41], [0.37], [0.37], [0.3], [0.26]]
        // let groupsTotalSize = 0
        // groupsTmp.forEach(rectangle => {
        //     rectangle.forEach(rectangle => {
        //         ++groupsTotalSize
        //     })
        // })
        // console.log("groupsTotalSize", groupsTotalSize)
        // function generateAllCombinationsWithRepetition(arr: number[], size: number, n: number, result: number[][] = [], current: number[] = []): number[][] {
        //     if (current.length === size) {
        //         result.push([...current])
        //         return result
        //     }
        //
        //     for (let i = n; i < arr.length; i++) {
        //         current.push(arr[i])
        //         generateAllCombinationsWithRepetition(arr, size, i, result, current)
        //         current.pop()
        //     }
        //
        //     return result
        // }
        //
        // function generateAllNumbersCombinationsWithRepetition(maxNumber: number, maxSize: number): number[][] {
        //     const numbers = Array.from({ length: maxNumber + 1 }, (_, i) => i)
        //     const result: number[][] = []
        //
        //     for (let i = 0; i <= maxNumber; i++) {
        //         const combinations = generateAllCombinationsWithRepetition(numbers, maxSize, i, [], [i])
        //         result.push(...combinations)
        //     }
        //
        //     return result
        // }
        //
        // // Example usage:
        // const maxNumber = groupsTmp.length
        // const maxSize = groupsTotalSize
        // const allNumbersCombinationsWithRepetition = generateAllNumbersCombinationsWithRepetition(maxNumber, maxSize)
        // console.log("All Numbers Combinations with Repetition:", allNumbersCombinationsWithRepetition)
        // console.log("solution size:", allNumbersCombinationsWithRepetition[0].length * allNumbersCombinationsWithRepetition.length)

        function generateConfigurations(groups: number[][]): number[][][] {
            const configurations: number[][][] = []

            for (const group of groups) {
                let remainingWidth = 1

                // Check each existing configuration
                for (const configuration of configurations) {
                    let canAddGroup = true

                    // Calculate the total width of rectangles in the current configuration
                    const totalWidth = configuration.reduce((sum, existingGroup) => sum + Math.max(...existingGroup), 0)

                    // Check if the current group can be added to the configuration without exceeding the limit
                    if (totalWidth + group[0] > 1) {
                        canAddGroup = false
                    }

                    // If the group can be added to the configuration, create a new configuration
                    if (canAddGroup) {
                        const newConfiguration = [...configuration, group]
                        configurations.push(newConfiguration)
                        remainingWidth = 1 // Reset the remaining width for the new configuration
                    }
                }

                // If the group couldn't be added to any existing configuration, create a new configuration
                if (remainingWidth >= group[0]) {
                    let shortGroup: number[] = []
                    shortGroup.push(group[0])
                    configurations.push([shortGroup])
                    // configurations.push([group])
                    remainingWidth -= group[0]
                }
            }

            return configurations
        }

        // Example usage:
        let groupsTmp: number[][] = [
            [1],
            [1, 1],
            [0.8],
            [0.7],
            [0.7],
            [0.68],
            [0.68, 0.68],
            [0.67],
            [0.66],
            [0.55],
            [0.49],
            [0.41],
            [0.37],
            [0.37],
            [0.3],
            [0.26]
        ]

        const generatedConfigurations = generateConfigurations(groupsTmp)
        console.log("Size: ", generatedConfigurations.length)
        console.log("Generated Configurations:", generatedConfigurations)

        // function processGroups(groups: number[][]): number[][] {
        //     const result: number[][] = []
        //     const totalGroups = groups.length
        //
        //     for (let i = 0; i < totalGroups; i++) {
        //         let currentSum = 0
        //         let currentGroup: number[] = []
        //
        //         for (let j = i; j < totalGroups; j++) {
        //             const currentElement = groups[j][0]
        //
        //             if (currentSum + currentElement <= 1) {
        //                 // If adding the current element doesn't exceed 1, include it in the current group
        //                 currentGroup.push(currentElement)
        //                 currentSum += currentElement
        //             } else {
        //                 // If adding the current element exceeds 1, start a new group
        //                 result.push(currentGroup)
        //                 currentGroup = [currentElement]
        //                 currentSum = currentElement
        //             }
        //         }
        //
        //         // Include the last group if it's not empty
        //         if (currentGroup.length > 0) {
        //             result.push(currentGroup)
        //         }
        //     }
        //
        //     return result
        // }
        //
        // // Example usage:
        // const processedGroups = processGroups(groupsTmp)
        // console.log("Processed Groups:", processedGroups)

        // const result = KenyonRemila(50, 50, tmp)

        // console.log(result)
    })
})
