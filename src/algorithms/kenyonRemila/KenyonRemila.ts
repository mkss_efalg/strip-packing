import {
    MarryRectangle,
    RectangleType,
    ShelfType,
    AdvancedShelfType,
    HorizontalSplitGroup,
    RectanglesConfiguration,
    RectangleWithGroupReferenceType,
    HorizontalSplitGroupVisited,
    HorizontalSpGroupConfiguration,
    TableuConfiguration
} from "../Algorithm.types"
import { deepCopy, getMaxWidth, sortDescendingHeight, sortDescendingWidth } from "../Algorithm.functions"

let storedContainerWidth: number
let storedContainerWidthHalf: number
// let storedH0: ShelfType
let startTime
let endTime

export default function KenyonRemila(containerWidth: number, rectangleCount: number, rectangles: RectangleType[]) {
    if (rectangles.length == 0) {
        return {
            sortedRectangles: [],
            markers: [],
            error: "No input rectangles present.",
            runTime: undefined
        }
    }
    const rectanglesCopy = deepCopy(rectangles)
    if (getMaxWidth(rectanglesCopy) > containerWidth) {
        return {
            sortedRectangles: [],
            markers: [],
            error: "Rectangles do not fit into the strip container.",
            runTime: undefined
        }
    }
    storedContainerWidth = containerWidth
    storedContainerWidthHalf = storedContainerWidth / 2

    startTime = performance.now()

    let epsilon: number = 1 / 4
    let epsilonStrich: number = epsilon / (2 + epsilon)

    //Prepare Rectangles & GroupSize
    const sortedRectangles = rectanglesCopy.sort(sortDescendingWidth)
    containerWidth = sortedRectangles[0].width
    const sortedScaledRectangles = scaleRectangles(sortedRectangles, containerWidth)
    console.debug("sortedScaledRectangles: ", sortedScaledRectangles)
    let horizontalGroupsOUT: HorizontalSplitGroup[] = []
    let groupedRectanglesOUT: RectangleWithGroupReferenceType[] = []
    let calcHorizontalSplittingGroupsRES = calcHorizontalSplittingGroups(sortedScaledRectangles, epsilon)
    console.debug("splitHeight: ", calcHorizontalSplittingGroupsRES.m)

    //Create Groups
    insertRecsInGroups(sortedScaledRectangles, calcHorizontalSplittingGroupsRES.m, horizontalGroupsOUT, groupedRectanglesOUT)
    // console.debug("groupedRectanglesOUT: ", groupedRectanglesOUT)
    console.debug("########## BEGIN")
    console.debug("horizontalGroupsOUT: ", horizontalGroupsOUT)
    horizontalGroupsOUT.forEach((group, groupIndex) => {
        console.debug(`Group ${groupIndex + 1}:`)
        console.debug(`Next Rectangle ID: ${group.groupID}`)
        console.debug("Rectangles:")
        group.rectangles.forEach((rectangle, index) => {
            console.debug(`  Rectangle ${index + 1}:`)
            console.debug(`    Width: ${rectangle.width}`)
            console.debug(`    Height: ${rectangle.height}`)
        })
        console.debug("") // Add a line break between groups for better readability
    }) //debug
    console.debug("########## END")

    let calculatedGroupAmount = calcHorizontalSplittingGroupsRES.totalRecHeight / calcHorizontalSplittingGroupsRES.m
    if (horizontalGroupsOUT.length == calculatedGroupAmount) {
        console.debug("GenCombinations")
        //Generate all posibile Combinations

        const allConfigurations = backtrackPossibleConfigurationsHelper(horizontalGroupsOUT, calcHorizontalSplittingGroupsRES.m) //horizontalGroupsOUT.length, horizontalGroupsOUT.length)
        // console.debug("All Numbers Combinations with Repetition:", allConfigurations)
        // console.debug("solution size:", allConfigurations[0].length * allConfigurations.length)
        makeConfiguration2Tableu(allConfigurations, calculatedGroupAmount)

        //Backtrack possible Configurations from posstible Combinations
        // let configuration: RectanglesConfiguration[] = []
        // backtrackPossibleConfigurationsHelper(groupedRectanglesOUT, splitHeight, configuration)
        // fitRectanglesIntoShelves(sortedScaledRectangles)
    } else {
        console.debug("Can't fill all ", calculatedGroupAmount, " Groups. Not enough Rectangles. groupedRectanglesOUT.length: ", groupedRectanglesOUT.length)
    }

    endTime = performance.now()

    return {
        sortedRectangles,
        markers: [],
        error: "",
        runTime: endTime - startTime
    }
}

function scaleRectangles(sortedRectangles: RectangleType[], containerWidth: number): RectangleType[] {
    let scaledRectangles: RectangleType[] = []
    let lastPosY = 0
    sortedRectangles.forEach((rectangle, index) => {
        let scaledRectangle = {
            id: index,
            posX: 0,
            posY: lastPosY,
            width: rectangle.width / containerWidth,
            height: rectangle.height
        }
        scaledRectangles.push(scaledRectangle)
        rectangle.posY = lastPosY
        rectangle.posX = 0
        lastPosY += scaledRectangle.height
    })
    return scaledRectangles
}

function calcEpsilonStrich(epsilon: number): number {
    return epsilon / (2 + epsilon)
}

function calcHorizontalSplittingGroups(sortedRectangles: RectangleType[], epsilon: number): { m: number; totalRecHeight: number } {
    let H: number = 1 / Math.pow(0.25, 2)
    let totalRecHeight = calcRectanglesTotalHeight(sortedRectangles)
    let m: number = totalRecHeight / H
    return { m, totalRecHeight }
}

function calcRectanglesTotalHeight(rectangles: RectangleType[]): number {
    let height = 0
    rectangles.forEach((rectangle, index) => {
        height += rectangle.height
    })
    console.debug("rectanglesMaxHeight: ", height)
    return height
}

function insertRecsInGroups(
    sortedRectangles: RectangleType[],
    splitHeight: number,
    horizonzalGroups: HorizontalSplitGroup[],
    groupRectangles: RectangleWithGroupReferenceType[]
) {
    console.debug('start debugging "insertRecsInGroups"')
    let groupID = 0
    let tmp: HorizontalSplitGroup = {
        groupID: groupID,
        rectangles: [sortedRectangles[0]],
        width: sortedRectangles[0].width
    } //debug
    horizonzalGroups.push(tmp)
    let height = 0
    let rectangleWithGroupReferenceType: RectangleWithGroupReferenceType = {
        rectangle: sortedRectangles[0],
        groupID: groupID,
        groupWidth: sortedRectangles[0].width
    }
    // groupRectangles.push(rectangleWithGroupReferenceType)
    let groupHeigth = sortedRectangles[0].height
    let i = 1
    for (; i < sortedRectangles.length; ++i) {
        height += sortedRectangles[i].height
        groupHeigth += sortedRectangles[i].height
        if (groupHeigth < splitHeight) {
            tmp.rectangles.push(sortedRectangles[i])
            rectangleWithGroupReferenceType.rectangle = sortedRectangles[i]
            groupRectangles.push(rectangleWithGroupReferenceType)
        } else {
            ++groupID
            tmp = {
                groupID: groupID,
                rectangles: [sortedRectangles[i]],
                width: sortedRectangles[i].width
            }
            horizonzalGroups.push(tmp)
            rectangleWithGroupReferenceType = {
                rectangle: sortedRectangles[i],
                groupID: groupID,
                groupWidth: sortedRectangles[i].width
            }
            groupRectangles.push(rectangleWithGroupReferenceType)
            groupHeigth = sortedRectangles[i].height
        }
    }
    // console.debug("########## BEGIN")
    // console.debug("i == sortedRectangles.len ? ", i, " == ", sortedRectangles.length)
    // console.debug("groupRectangles == configAmount", groupRectangles.length, " == ", height / splitHeight)
    // groupRectangles.forEach(value => {
    //     //debug
    //     console.debug("rectangle: ", value.rectangle, " set to ", value.groupID)
    // })
    // console.debug("########## END")
}

/*
 * Merge BEGIN
 * */
function generateConfigurations(groups: HorizontalSplitGroup[]): HorizontalSplitGroup[][] {
    const configurations: HorizontalSplitGroup[][] = []

    groups.forEach((group, index) => {
        let remainingWidth = 1

        // Check each existing configuration
        for (const configuration of configurations) {
            let canAddGroup = true

            // Calculate the total width of rectangles in the current configuration
            const totalWidth = configuration.reduce((sum, existingGroup) => sum + existingGroup.width!, 0)

            // Check if the current group can be added to the configuration without exceeding the limit
            if (totalWidth + group.width! > 1) {
                canAddGroup = false
            }

            // If the group can be added to the configuration, create a new configuration
            if (canAddGroup) {
                const newConfiguration = [...configuration, group]
                configurations.push(newConfiguration)
                remainingWidth = 1 // Reset the remaining width for the new configuration
            }
        }

        // If the group couldn't be added to any existing configuration, create a new configuration
        if (remainingWidth >= group.width!) {
            let shortGroup: HorizontalSplitGroup = {
                groupID: group.groupID,
                rectangles: [group.rectangles[0]],
                width: group.width
            }
            configurations.push([shortGroup])
            // configurations.push([group])
            remainingWidth -= group.width!
        }
    })

    return configurations
}

let tableu: TableuConfiguration[] = []

function backtrackPossibleConfigurationsHelper(groupedRectangles: HorizontalSplitGroup[], m: number) {
    // let configurations: RectanglesConfiguration[] = []
    // backtrackPossibleConfigurations(groupedRectangles, m, 0, 0, configurations)
    let configID = 0

    let configurations: HorizontalSplitGroup[][] = generateConfigurations(groupedRectangles)
    console.debug(configurations)
    let horizontalSpGroupConfiguration: HorizontalSpGroupConfiguration[] = []
    configurations.forEach((rectangle, index) => {
        let sumWidth = 0
        let ids: number[] = []
        rectangle.forEach((rectangle, index) => {
            sumWidth += rectangle.width!
            ids.push(rectangle.groupID)
        })
        let newConfig: HorizontalSpGroupConfiguration = {
            groupIDs: ids,
            sumWidth: sumWidth
        }
        horizontalSpGroupConfiguration.push(newConfig)
    })
    console.debug("horizontalSpGroupConfiguration: ", horizontalSpGroupConfiguration)

    return configurations
}

function makeConfiguration2Tableu(configurations: HorizontalSplitGroup[][], spalten: number) {
    for (let i = 0; i < configurations.length; ++i) {
        for (let n = 0; n < configurations[i].length; ++n) {
            // tableu[]
        }
    }
}

/*
 * Merge END
 * */

function tranformTableu() {
    let temp: number[][] = []
}

function simplex(minProblem: boolean = true) {
    let negativ = true
    if (minProblem) {
    }
}

function fitRectanglesIntoShelves(sortedRectangles: RectangleType[]) {
    const shelves: AdvancedShelfType[] = []
    fitRectanglesIntoShelf(sortedRectangles, shelves)
}

function fitRectanglesIntoShelf(sortedRectangles: RectangleType[], shelves: AdvancedShelfType[]) {
    const newSortedRectangles: RectangleType[] = []
    const h0leftBoundShelf: AdvancedShelfType = {
        posX: 0,
        posY: 0,
        width: storedContainerWidth,
        currentWidth: 0,
        currentHeight: 0,
        rectangles: []
    }
    let rectangleID: number = 0
}
