import { RectangleType, ShelfType } from "../Algorithm.types"
import { deepCopy, getMaxWidth, sortDescendingHeight } from "../Algorithm.functions"

let storedContainerWidth: number
let markers: number[]
let startTime
let endTime

export default function NextFitDecreasingHeight(containerWidth: number, rectangles: RectangleType[]) {
    if (rectangles.length == 0) {
        return {
            sortedRectangles: [],
            markers: [],
            error: "No input rectangles present.",
            runTime: undefined
        }
    }
    markers = []
    const rectanglesCopy = deepCopy(rectangles)
    if (getMaxWidth(rectanglesCopy) > containerWidth) {
        return {
            sortedRectangles: [],
            markers: [],
            error: "Rectangles do not fit into the strip container.",
            runTime: undefined
        }
    }

    storedContainerWidth = containerWidth

    startTime = performance.now()
    const sortedRectangles = rectanglesCopy.sort(sortDescendingHeight)
    fitRectanglesIntoShelves(sortedRectangles)
    endTime = performance.now()
    return {
        sortedRectangles,
        markers,
        error: "",
        runTime: endTime - startTime
    }
}

function fitRectanglesIntoShelves(sortedRectangles: RectangleType[]) {
    const shelves: ShelfType[] = []
    fitRectanglesIntoShelf(sortedRectangles, shelves)
    shelves.forEach(shelf => {
        markers.push(shelf.posY)
    })
}

function fitRectanglesIntoShelf(sortedRectangles: RectangleType[], shelves: ShelfType[]) {
    const shelf: ShelfType = {
        posY: 0,
        width: storedContainerWidth,
        currentWidth: 0,
        currentHeight: sortedRectangles[0].height,
        rectangles: []
    }
    shelves.push(shelf)
    let currentShelf = shelf
    sortedRectangles.forEach((rectangle, index) => {
        rectangle.posX = 0
        rectangle.posY = 0
        currentShelf = fitRectangleIntoShelf(shelves, currentShelf, rectangle)
    })
}

function fitRectangleIntoShelf(shelves: ShelfType[], shelf: ShelfType, rectangle: RectangleType): ShelfType {
    if (doesRectangleFitIntoShelf(shelf, rectangle)) {
        rectangle.posX = shelf.currentWidth
        rectangle.posY = shelf.posY
        shelf.rectangles.push(rectangle)
        shelf.currentWidth += rectangle.width
        return shelf
    } else {
        const newShelf: ShelfType = {
            posY: shelf.posY + shelf.currentHeight,
            width: storedContainerWidth,
            currentWidth: 0,
            currentHeight: rectangle.height,
            rectangles: []
        }
        shelves.push(newShelf)
        return fitRectangleIntoShelf(shelves, newShelf, rectangle)
    }
}

const doesRectangleFitIntoShelf = (shelf: ShelfType, rectangle: RectangleType) => {
    return (shelf.rectangles.length === 0 && rectangle.width <= shelf.width) || rectangle.width + shelf.currentWidth <= shelf.width
}
