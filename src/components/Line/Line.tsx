interface LineProps {
    posX: number
    posY: number
    width: number
    height: number
    label: string
    borderColor: string
    backgroundColor: string
    bottom: boolean
    center: boolean
    centerHeight: number
    centerHeightFrom: number
    left?: number
    fontSize: number
}

export default function Line(props: Readonly<LineProps>) {
    return (
        <div style={{ position: "inherit" }}>
            <div
                style={{
                    width: props.width + "px",
                    height: props.height + "px",
                    left: props.posX,
                    bottom: props.posY,
                    backgroundColor: props.backgroundColor,
                    borderColor: props.borderColor,
                    border: "0.1px solid " + props.borderColor,
                    borderRight: "0px",
                    borderTop: "0px",
                    position: "inherit",
                    boxSizing: "border-box",
                    zIndex: 10,
                    pointerEvents: "none"
                }}
            />
            <div
                style={{
                    left: props.left ? props.posX - props.width - props.left : props.posX + props.width,
                    bottom: props.center
                        ? props.centerHeightFrom + (props.centerHeight - props.centerHeightFrom) / 2
                        : props.bottom
                        ? props.posY - 10
                        : props.posY + props.height,
                    color: props.borderColor,
                    position: "absolute",
                    zIndex: 500,
                    boxSizing: "border-box",
                    display: "block",
                    borderRadius: "1px"
                }}
            >
                <div
                    style={{
                        fontSize: props.fontSize + "px",
                        whiteSpace: "pre-wrap",
                        textAlign: "center"
                    }}
                >
                    {props.label}
                </div>
            </div>
        </div>
    )
}
