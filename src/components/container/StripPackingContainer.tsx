import { Box } from "@mui/material"
import { RectangleType } from "../../algorithms/Algorithm.types"
import Rectangle from "../Rectangle/Rectangle"
import Line from "../Line/Line"
import { AlgorithmInfo, getHighestRectangle } from "../Frontend.function"
import LevelLine from "../LevelLine/LevelLine"
import React, { Dispatch, SetStateAction } from "react"
import SteinbergLegend from "../SteinbergLegend/SteinbergLegend"
import CompareAlgorithmsLine from "../CompareAlgorithmsLine/CompareAlgorithmsLine"

interface ContainerProps {
    view: string
    width: number
    height: number
    rectangles: RectangleType[]
    optimalRectangles: RectangleType[]
    optimalHeight: number
    opacity: number
    markers: number[]
    toggleDrawer: () => void
    setDrawerText: Dispatch<SetStateAction<string>>
    algorithmInfos: AlgorithmInfo[]
    heightTotal: number
    compareAlgorithms: boolean
    enableLevelLine: boolean
    enableSteinbergLegend: boolean
    enablePackedHeight: boolean
    enableOptimalHeight: boolean
}

export default function StripPackingContainer({
    view,
    width,
    height,
    rectangles,
    optimalRectangles,
    optimalHeight,
    opacity,
    markers,
    toggleDrawer,
    setDrawerText,
    algorithmInfos,
    heightTotal,
    compareAlgorithms,
    enableLevelLine,
    enableSteinbergLegend,
    enablePackedHeight,
    enableOptimalHeight
}: Readonly<ContainerProps>) {
    const optimalRectanglesOpacity = view === "packingAndOptimal" ? opacity / 100 : 1

    const highestPackedRectangle = getHighestRectangle(rectangles)
    const packedHeight = highestPackedRectangle ? highestPackedRectangle.posY! + highestPackedRectangle.height : 0

    const optimalHeightText =
        optimalHeight != packedHeight ? "Optimal (h:" + optimalHeight.toFixed(1) + ")" : "Optimal and packed (h:" + optimalHeight.toFixed(1) + ")"

    const steinbergColor = (steinbergMarker: string) => {
        if (steinbergMarker === "P1") {
            return "#ffc3ca"
        } else if (steinbergMarker === "P-1") {
            return "#ffe0b2"
        } else if (steinbergMarker === "P2") {
            return "#81d4fa"
        } else if (steinbergMarker === "P-2") {
            return "#fc9eff"
        } else if (steinbergMarker === "P0") {
            return "#8cef74"
        }

        return ""
    }

    const isSteinberg = rectangles.some(rectangle => rectangle.steinbergMarker !== undefined)

    let editedAlgorithmInfos: AlgorithmInfo[] = []
    algorithmInfos.forEach(algorithmInfo => {
        if (algorithmInfo.algorithm !== "kenyonRemila") {
            editedAlgorithmInfos.push(algorithmInfo)
        }
    })

    return (
        <Box
            width={width + "px"}
            height={height + "px"}
            borderColor="black"
            border={1 + "px solid"}
            borderTop={0}
            position="relative"
            display="flex"
            alignItems="flex-end"
            marginLeft={"8px"}
            marginRight={"8px"}
            marginTop={1}
            style={{ borderRadius: 1 }}
        >
            <div style={{ position: "absolute" }}>
                {(view === "packing" || view === "packingAndOptimal") &&
                    rectangles.map(rectangle => (
                        <Rectangle
                            key={rectangle.id}
                            rectangle={rectangle}
                            backgroundColor={
                                rectangle.steinbergMarker
                                    ? steinbergColor(rectangle.steinbergMarker)
                                    : rectangle.posY! + rectangle.height > height
                                    ? "#ffa200b0"
                                    : "lightgray"
                            }
                            borderColor="gray"
                            opacity={1}
                            steinberg={rectangle.steinbergMarker}
                            toggleDrawer={toggleDrawer}
                            setDrawerText={setDrawerText}
                            total={rectangles.length}
                        />
                    ))}
                {(view === "optimal" || view === "packingAndOptimal") &&
                    (view === "optimal" || opacity > 0) &&
                    !Number.isNaN(optimalHeight) &&
                    optimalRectangles.map(optimalRectangle => (
                        <Rectangle
                            key={optimalRectangle.id}
                            rectangle={optimalRectangle}
                            backgroundColor="indianred"
                            borderColor="darkred"
                            opacity={optimalRectanglesOpacity}
                            toggleDrawer={toggleDrawer}
                            setDrawerText={setDrawerText}
                            total={rectangles.length}
                        />
                    ))}
                {(enableOptimalHeight || (enablePackedHeight && optimalHeight === packedHeight)) &&
                    optimalRectangles.length > 0 &&
                    !Number.isNaN(optimalHeight) && (
                        <Line
                            label={optimalHeightText}
                            width={width + 10}
                            height={0.25}
                            posX={-5}
                            posY={optimalHeight}
                            backgroundColor="red"
                            borderColor="red"
                            bottom={true}
                            center={false}
                            centerHeight={0}
                            centerHeightFrom={0}
                            fontSize={3}
                        />
                    )}
                {enablePackedHeight && rectangles.length > 0 && optimalHeight != packedHeight && (
                    <Line
                        label={"Packed (h:" + packedHeight.toFixed(1) + ")"}
                        width={width + 10}
                        height={0.25}
                        posX={-5}
                        posY={packedHeight}
                        backgroundColor="purple"
                        borderColor="purple"
                        bottom={false}
                        center={false}
                        centerHeight={0}
                        centerHeightFrom={0}
                        fontSize={3}
                    />
                )}
                {enableLevelLine && markers.length > 0 && rectangles.length > 0 && (view === "packing" || view === "packingAndOptimal") && (
                    <LevelLine
                        markers={markers}
                        posX={-10}
                        posY={0}
                        width={0.4}
                        height={packedHeight}
                        label={"Level"}
                        borderColor={"black"}
                        backgroundColor={"black"}
                    />
                )}
                {enableSteinbergLegend && markers.length === 0 && rectangles.length > 0 && isSteinberg && <SteinbergLegend />}
                {rectangles.length > 0 && compareAlgorithms && (
                    <CompareAlgorithmsLine
                        algorithmInfos={editedAlgorithmInfos}
                        posX={width + 20}
                        posY={0}
                        width={0.4}
                        height={heightTotal}
                        label={"Algorithm Comparison"}
                        borderColor={"black"}
                        backgroundColor={"black"}
                    />
                )}
            </div>
        </Box>
    )
}
