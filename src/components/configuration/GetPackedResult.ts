import { RectangleType } from "../../algorithms/Algorithm.types"
import { ConfigurationType } from "./Configuration.types"

export default function GetPackedResult(stripWidth: number, packedHeight: number, startPacking: any) {
    return {
        stripWidth,
        packedHeight,
        packedResult: startPacking
    }
}
