import {RectangleType} from "../../algorithms/Algorithm.types";

export interface ConfigurationType {
    stripWidth: number
    stripHeight: number
    optimalRectangles: RectangleType[]
}