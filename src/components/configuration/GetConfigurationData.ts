import {RectangleType} from "../../algorithms/Algorithm.types"
import {ConfigurationType} from "./Configuration.types";

export default function GetConfigurationData(stripWidth: number, stripHeight: number, optimalRectangles: RectangleType[]): ConfigurationType {
    const optimalRectangleExport: RectangleType[] = []
    optimalRectangles.forEach(optimalRectangle => {
        optimalRectangleExport.push({
            id: optimalRectangle.id,
            width: optimalRectangle.width,
            height: optimalRectangle.height,
            posX: optimalRectangle.posX,
            posY: optimalRectangle.posY
        })
    })

    return {
        stripWidth,
        stripHeight,
        optimalRectangles: optimalRectangleExport
    }
}
