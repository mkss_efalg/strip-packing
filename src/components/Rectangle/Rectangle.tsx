import { RectangleType } from "../../algorithms/Algorithm.types"
import React, { Dispatch, SetStateAction, useState } from "react"

interface RectangleProps {
    rectangle: RectangleType
    borderColor: string
    backgroundColor: string
    opacity: number
    steinberg?: string
    toggleDrawer: () => void
    setDrawerText: Dispatch<SetStateAction<string>>
    total: number
}

export default function Rectangle(props: Readonly<RectangleProps>) {
    const [isHover, setIsHover] = useState(false)

    const onMouseHover = () => {
        setIsHover(true)
    }

    const onMouseHoverLeave = () => {
        setIsHover(false)
    }

    const onMouseClick = () => {
        props.toggleDrawer()
        if (props.setDrawerText) {
            props.setDrawerText(
                "Detailed Rectangle Information: \n ID: " +
                    props.rectangle.id +
                    "\n Width: " +
                    props.rectangle.width +
                    "\n Height: " +
                    props.rectangle.height +
                    "\n Pos X: " +
                    props.rectangle.posX +
                    "\n Pos Y: " +
                    props.rectangle.posY +
                    "\n Area: " +
                    props.rectangle.width * props.rectangle.height +
                    "\n Steinberg Procedure: " +
                    props.steinberg +
                    "\n Number of Rectangles: " +
                    props.total
            )
        }
    }

    const tooltipToggle = false

    const tooltipHeader = props.steinberg ? "<" + props.rectangle.id + ";" + props.steinberg + ">" : "<" + props.rectangle.id + ">"
    const tooltipDebug = "<d_id: " + props.rectangle.debug + ">"
    const tooltipText1 = props.rectangle.width.toFixed(1) + " x " + props.rectangle.height.toFixed(1)
    const tooltipText2 =
        props.rectangle.posX !== undefined && props.rectangle.posY !== undefined ? props.rectangle.posX.toFixed(1) + ";" + props.rectangle.posY.toFixed(1) : ""

    return (
        <div style={{ position: "inherit" }}>
            <div
                onMouseEnter={onMouseHover}
                onMouseLeave={onMouseHoverLeave}
                onClick={onMouseClick}
                style={{
                    width: props.rectangle.width + "px",
                    height: props.rectangle.height + "px",
                    left: props.rectangle.posX,
                    bottom: props.rectangle.posY,
                    backgroundColor: props.backgroundColor,
                    filter: isHover ? "brightness(110%)" : "brightness(100%)",
                    cursor: isHover ? "pointer" : "default",
                    borderColor: props.borderColor,
                    border: "0.1px solid " + props.borderColor,
                    borderRight: "0px",
                    borderTop: "0px",
                    position: "inherit",
                    boxSizing: "border-box",
                    zIndex: 10,
                    opacity: props.opacity
                }}
            />
            <div
                style={{
                    left: props.rectangle.posX! + props.rectangle.width / 2,
                    bottom: props.rectangle.posY! + props.rectangle.height + 0.5,
                    backgroundColor: "white",
                    borderColor: props.borderColor,
                    border: "0.1px solid",
                    position: "absolute",
                    zIndex: 700,
                    boxSizing: "border-box",
                    display: isHover ? "block" : "none",
                    borderRadius: "1px"
                }}
            >
                <div
                    style={{
                        fontSize: "3px",
                        whiteSpace: "nowrap",
                        textAlign: "center"
                    }}
                >
                    {tooltipHeader}
                </div>
                {tooltipToggle && (
                    <div
                        style={{
                            fontSize: "3px",
                            whiteSpace: "nowrap",
                            textAlign: "center"
                        }}
                    >
                        {tooltipDebug}
                    </div>
                )}
                <div
                    style={{
                        fontSize: "3px",
                        whiteSpace: "nowrap",
                        textAlign: "center"
                    }}
                >
                    {tooltipText1} <br />
                    {(props.rectangle.posX !== undefined || props.rectangle.posY !== undefined) && tooltipText2}
                </div>
            </div>
        </div>
    )
}
