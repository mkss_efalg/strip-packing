import Line from "../Line/Line"

export default function SteinbergLegend() {
    const borderColor = "gray"

    const posX = -11
    const posY = 6

    return (
        <div style={{ position: "inherit" }}>
            <div
                style={{
                    width: "5px",
                    height: "5px",
                    left: posX - 12,
                    bottom: 0,
                    backgroundColor: "#8cef74",
                    borderColor: borderColor,
                    border: "0.1px solid " + borderColor,
                    position: "inherit",
                    boxSizing: "border-box",
                    zIndex: 10,
                    pointerEvents: "none"
                }}
            />
            <div
                style={{
                    left: posX - 11.5,
                    bottom: 0,
                    color: borderColor,
                    position: "absolute",
                    zIndex: 500,
                    boxSizing: "border-box",
                    display: "block",
                    borderRadius: "1px"
                }}
            >
                <div
                    style={{
                        fontSize: "3px",
                        whiteSpace: "nowrap",
                        textAlign: "center"
                    }}
                >
                    P0
                </div>
            </div>
            <div
                style={{
                    width: "5px",
                    height: "5px",
                    left: posX,
                    bottom: 0,
                    backgroundColor: "#ffc3ca",
                    borderColor: borderColor,
                    border: "0.1px solid " + borderColor,
                    position: "inherit",
                    boxSizing: "border-box",
                    zIndex: 10,
                    pointerEvents: "none"
                }}
            />
            <div
                style={{
                    left: posX + 0.5,
                    bottom: 0,
                    color: borderColor,
                    position: "absolute",
                    zIndex: 500,
                    boxSizing: "border-box",
                    display: "block",
                    borderRadius: "1px"
                }}
            >
                <div
                    style={{
                        fontSize: "3px",
                        whiteSpace: "nowrap",
                        textAlign: "center"
                    }}
                >
                    P1
                </div>
            </div>
            <div
                style={{
                    width: "5px",
                    height: "5px",
                    left: posX,
                    bottom: posY,
                    backgroundColor: "#ffe0b2",
                    borderColor: borderColor,
                    border: "0.1px solid " + borderColor,
                    position: "inherit",
                    boxSizing: "border-box",
                    zIndex: 10,
                    pointerEvents: "none"
                }}
            />
            <div
                style={{
                    left: posX + 0.2,
                    bottom: posY,
                    color: borderColor,
                    position: "absolute",
                    zIndex: 500,
                    boxSizing: "border-box",
                    display: "block",
                    borderRadius: "1px"
                }}
            >
                <div
                    style={{
                        fontSize: "3px",
                        whiteSpace: "nowrap",
                        textAlign: "center"
                    }}
                >
                    P-1
                </div>
            </div>
            <div
                style={{
                    width: "5px",
                    height: "5px",
                    left: posX - 6,
                    bottom: 0,
                    backgroundColor: "#81d4fa",
                    borderColor: borderColor,
                    border: "0.1px solid " + borderColor,
                    position: "inherit",
                    boxSizing: "border-box",
                    zIndex: 10,
                    pointerEvents: "none"
                }}
            />
            <div
                style={{
                    left: posX - 5.5,
                    bottom: 0,
                    color: borderColor,
                    position: "absolute",
                    zIndex: 500,
                    boxSizing: "border-box",
                    display: "block",
                    borderRadius: "1px"
                }}
            >
                <div
                    style={{
                        fontSize: "3px",
                        whiteSpace: "nowrap",
                        textAlign: "center"
                    }}
                >
                    P2
                </div>
            </div>
            <div
                style={{
                    width: "5px",
                    height: "5px",
                    left: posX - 6,
                    bottom: posY,
                    backgroundColor: "#fc9eff",
                    borderColor: borderColor,
                    border: "0.1px solid " + borderColor,
                    position: "inherit",
                    boxSizing: "border-box",
                    zIndex: 10,
                    pointerEvents: "none"
                }}
            />
            <div
                style={{
                    left: posX - 5.8,
                    bottom: posY,
                    color: borderColor,
                    position: "absolute",
                    zIndex: 500,
                    boxSizing: "border-box",
                    display: "block",
                    borderRadius: "1px"
                }}
            >
                <div
                    style={{
                        fontSize: "3px",
                        whiteSpace: "nowrap",
                        textAlign: "center"
                    }}
                >
                    P-2
                </div>
            </div>
        </div>
    )
}
