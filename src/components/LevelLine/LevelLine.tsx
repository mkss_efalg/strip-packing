import Line from "../Line/Line"

interface LevelLineProps {
    posX: number
    posY: number
    width: number
    height: number
    label: string
    borderColor: string
    backgroundColor: string
    markers: number[]
}

export default function LevelLine(props: Readonly<LevelLineProps>) {
    let first = true
    const switchFirst = () => {
        first = false
        return 12
    }

    return (
        <div style={{ position: "inherit" }}>
            <div
                style={{
                    width: props.width + "px",
                    height: props.height + "px",
                    left: props.posX,
                    bottom: props.posY,
                    backgroundColor: props.backgroundColor,
                    borderColor: props.borderColor,
                    border: "0.1px solid " + props.borderColor,
                    borderRight: "0px",
                    borderTop: "0px",
                    position: "inherit",
                    boxSizing: "border-box",
                    zIndex: 10,
                    pointerEvents: "none"
                }}
            />
            <div
                style={{
                    left: props.posX - 5,
                    bottom: props.posY + props.height + 3,
                    color: props.borderColor,
                    position: "absolute",
                    zIndex: 500,
                    boxSizing: "border-box",
                    display: "block",
                    borderRadius: "1px"
                }}
            >
                <div
                    style={{
                        fontSize: "3px",
                        whiteSpace: "nowrap",
                        textAlign: "center"
                    }}
                >
                    {props.label}
                </div>
            </div>
            {props.markers.map((marker, index) => (
                <Line
                    key={index}
                    posX={props.posX - 2.5}
                    posY={marker}
                    width={5}
                    height={0.25}
                    label={index !== 0 ? "Level " + index + "(h:" + marker + ")" : ""}
                    borderColor={"black"}
                    backgroundColor={"black"}
                    bottom={false}
                    center={true}
                    centerHeight={marker}
                    centerHeightFrom={props.markers[index - 1]}
                    left={marker - props.markers[index - 1] <= 4 ? (first ? switchFirst() : index % 2 === 0 ? 30 : 12) : 12}
                    fontSize={2}
                />
            ))}
        </div>
    )
}
