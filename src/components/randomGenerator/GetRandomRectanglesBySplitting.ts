import { RectangleType } from "../../algorithms/Algorithm.types"

interface SplitPoint {
    pointX: number
    pointY: number
}

enum Split {
    x,
    y,
    none
}

/**
 * Function to get random rectangles by splitting a large one into smaller ones.
 *
 * @param width total width of the big rectangle
 * @param height total height of the big rectangle
 * @param posX position x
 * @param posY position y
 * @param numberOfSplits number of splits that should be performed (recursively)
 * @constructor
 */
export default function GetRandomRectanglesBySplitting(width: number, height: number, posX: number, posY: number, numberOfSplits: number): RectangleType[] {
    const splitRectangles = splitRectangle(width, height, posX, posY)

    const rectangleOne = splitRectangles[0]
    const rectangleTwo = splitRectangles.length > 1 ? splitRectangles[1] : undefined

    numberOfSplits--

    return [...recursiveCall(rectangleOne, numberOfSplits), ...(rectangleTwo ? recursiveCall(rectangleTwo, numberOfSplits) : [])]
}

/**
 * Recursive call to split further if condition is met.
 *
 * @param rectangle rectangle that should be split further if condition is met
 * @param numberOfSplits number of splits
 */
function recursiveCall(rectangle: RectangleType, numberOfSplits: number): RectangleType[] {
    if (checkIfRectangleCanBeSplit(rectangle) && numberOfSplits > 0) {
        return GetRandomRectanglesBySplitting(rectangle.width, rectangle.height, rectangle.posX!, rectangle.posY!, numberOfSplits)
    } else {
        return [rectangle]
    }
}

/**
 * Check if rectangle can be split (cant be split if it is a 1x1 rectangle).
 *
 * @param rectangle rectangle that should be checked
 */
function checkIfRectangleCanBeSplit(rectangle: RectangleType): boolean {
    return rectangle.width > 1 || rectangle.height > 1
}

/**
 * Split a rectangle by first determining a random point in the rectangle,
 * then decide randomly to split horizontally (y) or vertically (x).
 *
 * @param width width of the rectangle that should be split somewhere
 * @param height height of the rectangle that should be split somewhere
 * @param posX position x
 * @param posY position y
 */
function splitRectangle(width: number, height: number, posX: number, posY: number): RectangleType[] {
    const split = determineSplit(width, height)
    if (split == Split.none) {
        return [{ width, height, posX, posY }]
    }

    const rndPointForSplit: SplitPoint = {
        pointX: Math.floor(Math.random() * (width - 1)) + 1,
        pointY: Math.floor(Math.random() * (height - 1)) + 1
    }

    return split == Split.x
        ? [
              { width: rndPointForSplit.pointX, height: height, posX: posX, posY: posY },
              { width: width - rndPointForSplit.pointX, height: height, posX: posX + rndPointForSplit.pointX, posY: posY }
          ]
        : [
              { width: width, height: rndPointForSplit.pointY, posX: posX, posY: posY },
              { width: width, height: height - rndPointForSplit.pointY, posX: posX, posY: posY + rndPointForSplit.pointY }
          ]
}

/**
 * Decide what to split (x or y or in special case none).
 *
 * @param width width of rectangle to decide on
 * @param height height of the rectangle to decide on
 */
function determineSplit(width: number, height: number): Split {
    if (width == 1 && height == 1) {
        return Split.none
    } else if (width == 1) {
        return Split.y
    } else if (height == 1) {
        return Split.x
    }

    return Math.random() <= 0.5 ? Split.x : Split.y
}
