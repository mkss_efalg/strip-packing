import { AlgorithmInfo, getAlgorithmLabel, sortAlgorithmInfoAscendingHeight } from "../Frontend.function"
import CompareAlgorithmsInternalLine from "./CompareAlgorithmsInternalLine"

interface LevelLineProps {
    posX: number
    posY: number
    width: number
    height: number
    label: string
    borderColor: string
    backgroundColor: string
    algorithmInfos: AlgorithmInfo[]
}

export default function CompareAlgorithmsLine(props: Readonly<LevelLineProps>) {
    const filteredAlgorithmInfos = props.algorithmInfos
        .filter(algorithmInfo => algorithmInfo.runTime !== undefined && algorithmInfo.height !== undefined)
        .toSorted(sortAlgorithmInfoAscendingHeight)

    let algorithmInfoMap = new Map<number, AlgorithmInfo[]>()
    filteredAlgorithmInfos.forEach(algorithmInfo => {
        const value = algorithmInfoMap.get(algorithmInfo.height!)
        if (value) {
            value.push(algorithmInfo)
        } else {
            algorithmInfoMap.set(algorithmInfo.height!, [algorithmInfo])
        }
    })

    return (
        <div style={{ position: "inherit" }}>
            <div
                style={{
                    width: props.width + "px",
                    height: props.height + "px",
                    left: props.posX,
                    bottom: props.posY,
                    backgroundColor: props.backgroundColor,
                    borderColor: props.borderColor,
                    border: "0.1px solid " + props.borderColor,
                    borderRight: "0px",
                    borderTop: "0px",
                    position: "inherit",
                    boxSizing: "border-box",
                    zIndex: 10,
                    pointerEvents: "none"
                }}
            />
            <div
                style={{
                    left: props.posX - 2,
                    bottom: props.posY + props.height + 3,
                    color: props.borderColor,
                    position: "absolute",
                    zIndex: 500,
                    boxSizing: "border-box",
                    display: "block",
                    borderRadius: "1px"
                }}
            >
                <div
                    style={{
                        fontSize: "3px",
                        whiteSpace: "nowrap",
                        textAlign: "center"
                    }}
                >
                    {props.label}
                </div>
            </div>
            {Array.from(algorithmInfoMap.entries()).map(([key, algorithmInfo], index) => (
                <CompareAlgorithmsInternalLine
                    key={key}
                    posX={props.posX - 2.5}
                    posY={key}
                    width={5}
                    height={0.25}
                    label={getAlgorithmLabel(algorithmInfo)}
                    borderColor={"black"}
                    backgroundColor={"black"}
                    left={index % 2 === 0 ? 6 : 35}
                    fontSize={2}
                />
            ))}
        </div>
    )
}
