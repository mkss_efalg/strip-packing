interface LineProps {
    posX: number
    posY: number
    width: number
    height: number
    label: string
    borderColor: string
    backgroundColor: string
    left: number
    fontSize: number
}

export default function CompareAlgorithmsInternalLine(props: Readonly<LineProps>) {
    return (
        <div style={{ position: "inherit" }}>
            <div
                style={{
                    width: props.width + "px",
                    height: props.height + "px",
                    left: props.posX,
                    bottom: props.posY,
                    backgroundColor: props.backgroundColor,
                    borderColor: props.borderColor,
                    border: "0.1px solid " + props.borderColor,
                    borderRight: "0px",
                    borderTop: "0px",
                    position: "inherit",
                    boxSizing: "border-box",
                    zIndex: 10,
                    pointerEvents: "none"
                }}
            />
            <div
                style={{
                    left: props.posX + props.left,
                    bottom: props.posY + props.height - 1,
                    color: props.borderColor,
                    width: "100%",
                    position: "absolute",
                    zIndex: 500,
                    boxSizing: "border-box",
                    display: "block",
                    borderRadius: "1px"
                }}
            >
                <div
                    style={{
                        fontSize: props.fontSize + "px",
                        color: "purple",
                        whiteSpace: "nowrap",
                        textAlign: "center"
                    }}
                >
                    {props.label}
                </div>
            </div>
        </div>
    )
}
