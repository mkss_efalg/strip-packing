import { RectangleType } from "../algorithms/Algorithm.types"
import { ConfigurationType } from "./configuration/Configuration.types"

export interface AlgorithmInfo {
    algorithm: string
    runTime?: number
    height?: number
}

export function filter(rectangle: RectangleType, searchRectangle: string, rectangleSearchProperty: string) {
    if (searchRectangle.length === 0) {
        return true
    }

    if (rectangleSearchProperty === "id") {
        return rectangle.id === +searchRectangle
    } else if (rectangleSearchProperty === "width") {
        return rectangle.width === +searchRectangle
    } else if (rectangleSearchProperty === "height") {
        return rectangle.height === +searchRectangle
    }

    return rectangle.id === +searchRectangle || rectangle.width === +searchRectangle || rectangle.height === +searchRectangle
}

export function sort(rectangleProperty: string, rectangleSort: string) {
    if (rectangleProperty === "id") {
        if (rectangleSort === "ascending") {
            return sortAscendingId
        } else if (rectangleSort === "descending") {
            return sortDescendingId
        }
    } else if (rectangleProperty === "width") {
        if (rectangleSort === "ascending") {
            return sortAscendingWidth
        } else if (rectangleSort === "descending") {
            return sortDescendingWidth
        }
    } else if (rectangleProperty === "height") {
        if (rectangleSort === "ascending") {
            return sortAscendingHeight
        } else if (rectangleSort === "descending") {
            return sortDescendingHeight
        }
    }
}

export function getHighestRectangle(rectangles: RectangleType[]) {
    if (rectangles.length > 0) {
        return rectangles.reduce((prev, current) => (prev.posY! + prev.height > current.posY! + current.height ? prev : current))
    }
}

export function importZdf(data: string[]): ConfigurationType {
    const optimalRectangles: RectangleType[] = []
    for (let i = 2; i < data.length - 1; i++) {
        const row = data[i]
        const rowData = row.split(" ")
        if (rowData.length > 3) {
            throw Error
        }
        const id = +rowData[0]
        const width = +rowData[1]
        const height = +rowData[2]
        optimalRectangles.push({
            id,
            width,
            height
        })
    }
    return {
        stripWidth: +data[1],
        stripHeight: 1,
        optimalRectangles
    }
}

export function getAlgorithmLabel(algorithmInfos: AlgorithmInfo[]) {
    let multipleTranslations = ""
    algorithmInfos.forEach(algorithmInfo => {
        multipleTranslations += translateAlgorithmInfo(algorithmInfo) + ","
    })

    const cuttedMultipleTranslations = multipleTranslations.substring(0, multipleTranslations.length - 1)

    return cuttedMultipleTranslations + "(h:" + algorithmInfos[0].height!.toFixed(1) + ")"
}

export function translateAlgorithmInfo(algorithmInfo: AlgorithmInfo) {
    return translateAlgorithmShort(algorithmInfo.algorithm)
}

export function translateAlgorithmShort(algorithm: string) {
    if (algorithm === "nextFitDecreasingHeight") {
        return "NFDH"
    } else if (algorithm === "firstFitDecreasingHeight") {
        return "FFDH"
    } else if (algorithm === "reverseFit") {
        return "RF"
    } else if (algorithm === "steinberg") {
        return "ST"
    } else if (algorithm === "steinbergWithDropPacking") {
        return "ST-DP"
    } else if (algorithm === "kenyonRemila") {
        return "KR"
    }

    return "unknown"
}

export function translateAlgorithm(algorithm: string) {
    if (algorithm === "nextFitDecreasingHeight") {
        return "Next-Fit-Decreasing-Height"
    } else if (algorithm === "firstFitDecreasingHeight") {
        return "First-Fit-Decreasing-Height"
    } else if (algorithm === "reverseFit") {
        return "Reverse-Fit"
    } else if (algorithm === "steinberg") {
        return "Steinberg"
    } else if (algorithm === "steinbergWithDropPacking") {
        return "Steinberg Drop Packing Heuristic"
    } else if (algorithm === "kenyonRemila") {
        return "Kenyon Rémila"
    }

    return "unknown"
}

export const sortAlgorithmInfoAscendingHeight = (algorithmInfoA: AlgorithmInfo, algorithmInfoB: AlgorithmInfo) =>
    algorithmInfoA.height! - algorithmInfoB.height!

export const sortAscendingId = (rectangleA: RectangleType, rectangleB: RectangleType) => rectangleA.id! - rectangleB.id!
export const sortDescendingId = (rectangleA: RectangleType, rectangleB: RectangleType) => rectangleB.id! - rectangleA.id!

export const sortAscendingWidth = (rectangleA: RectangleType, rectangleB: RectangleType) => rectangleA.width - rectangleB.width
export const sortDescendingWidth = (rectangleA: RectangleType, rectangleB: RectangleType) => rectangleB.width - rectangleA.width

const sortAscendingHeight = (rectangleA: RectangleType, rectangleB: RectangleType) => rectangleA.height - rectangleB.height
const sortDescendingHeight = (rectangleA: RectangleType, rectangleB: RectangleType) => rectangleB.height - rectangleA.height
