import React from 'react'
import {ThemeProvider} from '@mui/material'
import RootPage from './pages/rootPage/RootPage'
import theme from './App.theme'
import {SnackbarProvider} from "notistack";

function App() {
    return (
        <ThemeProvider theme={theme}>
            <SnackbarProvider style={{ whiteSpace: "pre-wrap" }}>
                <RootPage />
            </SnackbarProvider>
        </ThemeProvider>
    )
}

export default App
