import { Box, Button, Dialog, DialogActions, DialogContent, DialogContentText, DialogTitle, Paper, PaperProps } from "@mui/material"
import Draggable from "react-draggable"
import React, { Dispatch, SetStateAction } from "react"
import HideSourceIcon from "@mui/icons-material/HideSource"
import { RectangleType } from "../../../../algorithms/Algorithm.types"
import Rectangle from "../../../../components/Rectangle/Rectangle"

interface OptimalRectanglesDialogProps {
    open: boolean
    handleClose: () => void
    optimalRectangles: RectangleType[]
    optimalHeight: number
    stripWidth: number
    scale: number
    toggleDrawer: () => void
    setDrawerText: Dispatch<SetStateAction<string>>
}

function PaperComponent(props: PaperProps) {
    return (
        <Draggable handle="#optimal-rectangles-dialog" cancel={'[class*="MuiDialogContent-root"]'}>
            <Paper {...props} />
        </Draggable>
    )
}

export default function OptimalRectanglesDialog({
    open,
    handleClose,
    optimalRectangles,
    optimalHeight,
    stripWidth,
    scale,
    toggleDrawer,
    setDrawerText
}: Readonly<OptimalRectanglesDialogProps>) {
    return (
        <Dialog open={open} onClose={handleClose} aria-labelledby="optimal-rectangles-dialog" PaperComponent={PaperComponent} sx={{ zIndex: 1000 }}>
            <DialogTitle sx={{ cursor: "move" }}>Optimal Rectangles</DialogTitle>
            <DialogContent>
                <DialogContentText whiteSpace="pre-line">
                    {Number.isNaN(optimalHeight)
                        ? "No optimal rectangles are provided"
                        : "These are the configured optimally positioned rectangles.\n Optimal height is at: " + optimalHeight}
                </DialogContentText>
                {!Number.isNaN(optimalHeight) && (
                    <Box
                        width={stripWidth + "px"}
                        height={optimalHeight + "px"}
                        position="relative"
                        display="flex"
                        alignItems="flex-end"
                        marginTop={2}
                        marginRight={2}
                        sx={{ zoom: scale }}
                    >
                        <div style={{ position: "absolute" }}>
                            {optimalRectangles.map(optimalRectangle => (
                                <Rectangle
                                    key={optimalRectangle.id}
                                    rectangle={optimalRectangle}
                                    backgroundColor="indianred"
                                    borderColor="darkred"
                                    opacity={1}
                                    toggleDrawer={toggleDrawer}
                                    setDrawerText={setDrawerText}
                                    total={optimalRectangles.length}
                                />
                            ))}
                        </div>
                    </Box>
                )}
            </DialogContent>
            <DialogActions>
                <Button variant="contained" color="secondary" onClick={handleClose} startIcon={<HideSourceIcon />}>
                    Hide
                </Button>
            </DialogActions>
        </Dialog>
    )
}
