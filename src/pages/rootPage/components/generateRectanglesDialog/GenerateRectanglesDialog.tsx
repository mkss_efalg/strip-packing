import { Box, Button, Dialog, DialogActions, DialogContent, DialogContentText, DialogTitle, InputAdornment, Paper, PaperProps, TextField } from "@mui/material"
import Draggable from "react-draggable"
import React, { ChangeEvent, Dispatch, SetStateAction, useEffect } from "react"
import SplitscreenTwoToneIcon from "@mui/icons-material/SplitscreenTwoTone"
import HeightIcon from "@mui/icons-material/Height"
import RefreshIcon from "@mui/icons-material/Refresh"
import HideSourceIcon from "@mui/icons-material/HideSource"

const RECTANGLE_HEIGHT_MIN = 1
const NUMBER_OF_SPLITS_MIN = 1
const NUMBER_OF_SPLITS_MAX = 16

interface GenerateRectanglesDialogProps {
    open: boolean
    handleClose: () => void
    handleGenerate: () => void
    rectangleHeightMax: number
    rectangleHeight: number
    setRectangleHeight: Dispatch<SetStateAction<number>>
    numberOfSplits: number
    setNumberOfSplits: Dispatch<SetStateAction<number>>
    dynamicHeight: boolean
}

function PaperComponent(props: PaperProps) {
    return (
        <Draggable handle="#generate-rectangles-dialog" cancel={'[class*="MuiDialogContent-root"]'}>
            <Paper {...props} />
        </Draggable>
    )
}

export default function GenerateRectanglesDialog({
    open,
    handleClose,
    handleGenerate,
    rectangleHeightMax,
    rectangleHeight,
    setRectangleHeight,
    numberOfSplits,
    setNumberOfSplits,
    dynamicHeight
}: Readonly<GenerateRectanglesDialogProps>) {
    const handleRectangleHeight = (e: ChangeEvent<HTMLInputElement>) => {
        const newRectangleHeight = +e.target.value
        if (newRectangleHeight >= RECTANGLE_HEIGHT_MIN && (!dynamicHeight ? newRectangleHeight <= rectangleHeightMax : true)) {
            setRectangleHeight(+e.target.value)
        }
    }

    const handleNumberOfSplits = (e: ChangeEvent<HTMLInputElement>) => {
        const newNumberOfSplits = +e.target.value
        if (newNumberOfSplits >= NUMBER_OF_SPLITS_MIN && newNumberOfSplits <= NUMBER_OF_SPLITS_MAX) {
            setNumberOfSplits(+e.target.value)
        }
    }

    useEffect(() => {
        if (open) {
            if (!dynamicHeight && rectangleHeight > rectangleHeightMax) {
                setRectangleHeight(rectangleHeightMax)
            }
        }
    }, [open])

    return (
        <Dialog open={open} onClose={handleClose} aria-labelledby="generate-rectangles-dialog" PaperComponent={PaperComponent} sx={{ zIndex: 1000 }}>
            <DialogTitle sx={{ cursor: "move" }}>Generate Rectangles</DialogTitle>
            <DialogContent>
                <DialogContentText>
                    Generate small rectangles by splitting a large one into smaller pieces. <br /> <br />
                    The width is fixed (as wide as strip width). The height and the number of splits (recursively) can be configured. <br /> <br />
                    In the Rectangle-Viewer tab (at the top of web page), the generated rectangles can be inspected. <br /> <br />
                </DialogContentText>
                <Box component="form" noValidate autoComplete="off">
                    <TextField
                        onChange={handleRectangleHeight}
                        autoFocus
                        helperText={`${RECTANGLE_HEIGHT_MIN} - ${!dynamicHeight ? rectangleHeightMax : "∞"}`}
                        margin="dense"
                        id="rectangleHeight"
                        label="Rectangle height"
                        type="number"
                        value={rectangleHeight}
                        fullWidth
                        variant="standard"
                        InputProps={{
                            startAdornment: (
                                <InputAdornment position="start">
                                    <HeightIcon />
                                </InputAdornment>
                            )
                        }}
                    />
                    <TextField
                        onChange={handleNumberOfSplits}
                        helperText={`${NUMBER_OF_SPLITS_MIN} - ${NUMBER_OF_SPLITS_MAX}`}
                        margin="dense"
                        id="numberOfSplits"
                        label="Number of splits (recursively)"
                        type="number"
                        value={numberOfSplits}
                        fullWidth
                        variant="standard"
                        InputProps={{
                            startAdornment: (
                                <InputAdornment position="start">
                                    <SplitscreenTwoToneIcon />
                                </InputAdornment>
                            )
                        }}
                    />
                </Box>
            </DialogContent>
            <DialogActions>
                <Button variant="contained" color="secondary" onClick={handleClose} startIcon={<HideSourceIcon />}>
                    Hide
                </Button>
                <Button variant="contained" color="success" onClick={handleGenerate} endIcon={<RefreshIcon />}>
                    Generate
                </Button>
            </DialogActions>
        </Dialog>
    )
}
