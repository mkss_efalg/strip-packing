import React, { ChangeEvent } from "react"
import { Checkbox, FormControlLabel, FormGroup, Stack, Tooltip, Typography } from "@mui/material"

interface AlgorithmConfigurationProps {
    handleSteinbergHeuristicChange: (e: ChangeEvent<HTMLInputElement>) => void
    steinbergHeuristic: boolean
    handleCompareAlgorithmsChange: (e: ChangeEvent<HTMLInputElement>) => void
    compareAlgorithms: boolean
    handleDisableVisualizationChange: (e: ChangeEvent<HTMLInputElement>) => void
    disableVisualization: boolean
    handleDisableSteinbergHeuristicChange: (e: ChangeEvent<HTMLInputElement>) => void
    disableSteinbergHeuristic: boolean
}

export default function AlgorithmConfiguration({
    handleSteinbergHeuristicChange,
    steinbergHeuristic,
    handleCompareAlgorithmsChange,
    compareAlgorithms,
    handleDisableVisualizationChange,
    disableVisualization,
    handleDisableSteinbergHeuristicChange,
    disableSteinbergHeuristic
}: AlgorithmConfigurationProps) {
    return (
        <Stack>
            <Stack direction="row">
                <Tooltip
                    title="Steinberg Heuristic to drop the floating packages until it hits another package or ground. This decreases the packed height and also the performance/runtime."
                    enterDelay={750}
                    enterNextDelay={750}
                    disableInteractive
                >
                    <FormControlLabel
                        control={<Checkbox checked={steinbergHeuristic} onChange={handleSteinbergHeuristicChange} />}
                        label={<Typography sx={{ fontSize: "12px" }}>Steinberg Drop Packing Heuristic</Typography>}
                    />
                </Tooltip>
                <Tooltip
                    title="Compare all algorithms. This will enable to the right of the strip container a line that shows the packed height of all algorithms. In the Algorithm Details dialog it will enable the runtime comparison between the Algorithms."
                    enterDelay={750}
                    enterNextDelay={750}
                    disableInteractive
                >
                    <FormControlLabel
                        control={<Checkbox checked={compareAlgorithms} onChange={handleCompareAlgorithmsChange} />}
                        label={<Typography sx={{ fontSize: "12px" }}>Compare with other Algorithms</Typography>}
                    />
                </Tooltip>
            </Stack>
            <Stack direction="row">
                <Tooltip
                    title="Disable all visualization representation (strip packing container and rectangle-viewer). Instead raw text in form of JSON representation will be used."
                    enterDelay={750}
                    enterNextDelay={750}
                    disableInteractive
                >
                    <FormControlLabel
                        control={<Checkbox checked={disableVisualization} onChange={handleDisableVisualizationChange} />}
                        label={<Typography sx={{ fontSize: "12px" }}>Disable Visualization</Typography>}
                    />
                </Tooltip>
                <Tooltip
                    title="Disable Steinberg Drop Packing Heuristic for comparison (when enabling 'Compare with other Algorithms')."
                    enterDelay={750}
                    enterNextDelay={750}
                    disableInteractive
                >
                    <FormControlLabel
                        control={<Checkbox checked={disableSteinbergHeuristic} onChange={handleDisableSteinbergHeuristicChange} />}
                        label={<Typography sx={{ fontSize: "12px" }}>Disable Steinberg Drop Packing for comparison</Typography>}
                    />
                </Tooltip>
            </Stack>
        </Stack>
    )
}
