export const INITIAL_SCALE = 6.5
export const SCALE_MIN = 1
export const SCALE_MAX = 10
export const SCALE_STEP = 0.5
