import { Button, Slider, Stack, Typography } from "@mui/material"
import React, { MouseEventHandler } from "react"
import { SCALE_MAX, SCALE_MIN, SCALE_STEP } from "./ScalePanel.constants"
import ZoomPanelStyles from "../zoomPanel/ZoomPanel.styles"

interface ZoomPanelProps {
    scale: number
    scaleSliderHandler: (event: Event, newValue: number | number[]) => void
    onResetScale: MouseEventHandler
}

export default function ScalePanel({ scale, scaleSliderHandler, onResetScale }: Readonly<ZoomPanelProps>) {
    return (
        <Stack
            display="flex"
            justifyContent="center"
            alignItems="center"
            sx={{
                outline: "1px solid",
                borderRadius: "5px",
                padding: "5px",
                paddingLeft: "10px",
                paddingRight: "10px",
                boxShadow: "3px 3px 3px gray",
                marginRight: "10px"
            }}
        >
            <Typography variant="h6" noWrap component="span" sx={ZoomPanelStyles.text}>
                Scale: {`${scale}`}
            </Typography>
            <Slider
                value={scale}
                onChange={scaleSliderHandler}
                step={SCALE_STEP}
                min={SCALE_MIN}
                max={SCALE_MAX}
                sx={{ width: "90%", marginTop: "5px", marginBottom: "5px" }}
            />
            <Button variant="contained" color="secondary" onClick={onResetScale} sx={{ width: "100px" }}>
                Reset
            </Button>
        </Stack>
    )
}
