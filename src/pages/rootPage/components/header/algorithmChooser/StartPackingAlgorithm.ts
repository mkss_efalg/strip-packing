import NextFitDecreasingHeight from "../../../../../algorithms/nextFitDecreasingHeight/NextFitDecreasingHeight"
import FirstFitDecreasingHeight from "../../../../../algorithms/firstFitDecreasingHeight/FirstFitDecreasingHeight"
import { RectangleType } from "../../../../../algorithms/Algorithm.types"
import ReverseFit from "../../../../../algorithms/reverseFit/ReverseFit"
import Steinberg from "../../../../../algorithms/steinberg/Steinberg"
import KenyonRemila from "../../../../../algorithms/kenyonRemila/KenyonRemila"

export default function StartPackingAlgorithm(
    algorithm: string,
    stripWidth: number,
    stripHeight: number,
    inputRectangles: RectangleType[],
    steinbergHeuristic: boolean,
    enableSteinbergInverseFirst: boolean,
    dynamicHeight: boolean
) {
    if (algorithm === "nextFitDecreasingHeight") {
        return NextFitDecreasingHeight(stripWidth, inputRectangles)
    } else if (algorithm === "firstFitDecreasingHeight") {
        return FirstFitDecreasingHeight(stripWidth, inputRectangles)
    } else if (algorithm === "reverseFit") {
        return ReverseFit(stripWidth, inputRectangles)
    } else if (algorithm === "steinberg") {
        return Steinberg(stripWidth, !dynamicHeight ? stripHeight : undefined, inputRectangles, steinbergHeuristic, enableSteinbergInverseFirst)
    } else if (algorithm === "kenyonRemila") {
        return KenyonRemila(stripWidth, stripHeight, inputRectangles)
    }

    return {
        sortedRectangles: [],
        markers: [],
        error: "",
        runTime: undefined
    }
}
