import React, { ChangeEvent, Dispatch, SetStateAction } from "react"
import { Box, Checkbox, FormControl, FormControlLabel, FormLabel, Radio, RadioGroup, Stack, Tooltip, Typography } from "@mui/material"
import WarningIcon from "@mui/icons-material/Warning"

interface AlgorithmChooserProps {
    algorithm: string
    setAlgorithm: Dispatch<SetStateAction<string>>
    handleEnableSteinbergInverseFirstChange: (e: ChangeEvent<HTMLInputElement>) => void
    enableSteinbergInverseFirst: boolean
}

export default function AlgorithmChooser({
    algorithm,
    setAlgorithm,
    handleEnableSteinbergInverseFirstChange,
    enableSteinbergInverseFirst
}: Readonly<AlgorithmChooserProps>) {
    const handleChange = (e: ChangeEvent<HTMLInputElement>) => {
        setAlgorithm(e.target.value)
    }

    return (
        <Box sx={{ display: "flex" }}>
            <FormControl component="fieldset" variant="standard">
                <FormLabel component="legend">Choose an algorithm:</FormLabel>
                <RadioGroup aria-label="algorithm" name="algorithm" value={algorithm} onChange={handleChange}>
                    <Stack direction="row">
                        <Tooltip title="Next-Fit-Decreasing-Height" enterDelay={750} enterNextDelay={750} disableInteractive>
                            <FormControlLabel value="nextFitDecreasingHeight" control={<Radio />} label="NFDH" />
                        </Tooltip>
                        <Tooltip title="First-Fit-Decreasing-Height" enterDelay={750} enterNextDelay={750} disableInteractive>
                            <FormControlLabel value="firstFitDecreasingHeight" control={<Radio />} label="FFDH" />
                        </Tooltip>
                        <Tooltip
                            title="ST, and when Heuristic is enabled => ST-DP (Steinberg with Drop Packing Heuristic enabled)"
                            enterDelay={750}
                            enterNextDelay={750}
                            disableInteractive
                        >
                            <FormControlLabel value="steinberg" control={<Radio />} label="Steinberg" />
                        </Tooltip>
                    </Stack>
                    <Stack direction="row">
                        <Tooltip title="RF" enterDelay={750} enterNextDelay={750} disableInteractive>
                            <FormControlLabel value="reverseFit" control={<Radio />} label="Reverse-Fit" />
                        </Tooltip>
                        <Tooltip title="KR, not working (implementation missing)" enterDelay={750} enterNextDelay={750} disableInteractive>
                            <FormControlLabel
                                value="kenyonRemila"
                                control={<Radio />}
                                label={
                                    <Stack>
                                        <Stack spacing={1} direction="row">
                                            <div>Kenyon Rémila</div>
                                            <WarningIcon sx={{ color: "orange" }} />
                                        </Stack>
                                    </Stack>
                                }
                            />
                        </Tooltip>
                    </Stack>
                    <Stack direction="row">
                        <Tooltip
                            title="Enable Inverse First for Steinberg. When disabled the standard procedure sequence is => P1, P-1, P3, P-3, P2, P-2, P0. When enabled the procedure sequence will use the inverse procedure before the non-inverse one => P-1, P1, P-3, P3, P-2, P2, P0"
                            enterDelay={750}
                            enterNextDelay={750}
                            disableInteractive
                        >
                            <FormControlLabel
                                control={<Checkbox checked={enableSteinbergInverseFirst} onChange={handleEnableSteinbergInverseFirstChange} />}
                                label={<Typography>Enable Steinberg Inverse First</Typography>}
                            />
                        </Tooltip>
                    </Stack>
                </RadioGroup>
            </FormControl>
        </Box>
    )
}
