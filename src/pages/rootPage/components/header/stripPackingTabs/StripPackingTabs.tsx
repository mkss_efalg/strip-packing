import { Box, Stack, Tab, Tabs, Tooltip } from "@mui/material"
import { SyntheticEvent } from "react"
import DashboardIcon from "@mui/icons-material/Dashboard"
import RectangleIcon from "@mui/icons-material/Rectangle"
import InfoIcon from "@mui/icons-material/Info"
import FolderIcon from "@mui/icons-material/Folder"
import HistoryIcon from "@mui/icons-material/History"
import VisibilityIcon from "@mui/icons-material/Visibility"

interface StripPackingTabsProps {
    tabIndex: number
    onTabIndexChangeHandler: (e: SyntheticEvent, newValue: number) => void
}

function a11yProps(index: number) {
    return {
        id: `simple-tab-${index}`,
        "aria-controls": `simple-tabpanel-${index}`
    }
}

export default function StripPackingTabs(stripPackingTabsProps: Readonly<StripPackingTabsProps>) {
    return (
        <Box>
            <Tabs value={stripPackingTabsProps.tabIndex} onChange={stripPackingTabsProps.onTabIndexChangeHandler} variant="scrollable" scrollButtons="auto">
                <Tooltip title="Main tab which shows the strip container and some overlay options." enterDelay={750} enterNextDelay={750} disableInteractive>
                    <Tab icon={<DashboardIcon />} label="strip-packing" {...a11yProps(0)} />
                </Tooltip>
                <Tooltip
                    title="Rectangle-Viewer tab which shows all the generated or imported rectangles. Some filter and order options are available. It is also possible to view the optimal rectangle constellation if provided."
                    enterDelay={750}
                    enterNextDelay={750}
                    disableInteractive
                >
                    <Tab
                        icon={
                            <Stack direction="row">
                                <VisibilityIcon />
                                <RectangleIcon />
                            </Stack>
                        }
                        label="Rectangle-Viewer"
                        {...a11yProps(1)}
                    />
                </Tooltip>
                <Tooltip
                    title="Shows some general information and a manual about the app. Strip packing and the implemented algorithms will also be roughly explained."
                    enterDelay={750}
                    enterNextDelay={750}
                    disableInteractive
                >
                    <Tab icon={<InfoIcon />} label="About" {...a11yProps(2)} />
                </Tooltip>
            </Tabs>
        </Box>
    )
}
