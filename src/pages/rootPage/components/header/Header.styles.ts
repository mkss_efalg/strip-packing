import theme from "../../../../App.theme"

const HeaderBorderWith = {
    bottom: 1,
    top: 0
}

const HeaderStyles = {
    AppBar: {
        zIndex: 900,
        boxShadow: "none",
        borderWidth: `${HeaderBorderWith.top}px 0 ${HeaderBorderWith.bottom}px 0`,
        borderStyle: "solid",
        borderColor: theme.palette.divider,
        overflowX: "auto"
    }
}

export default HeaderStyles
