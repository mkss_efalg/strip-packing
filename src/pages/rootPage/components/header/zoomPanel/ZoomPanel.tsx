import {Button, Slider, Stack, Typography} from "@mui/material"
import React, {MouseEventHandler} from "react"
import ZoomPanelStyles from "./ZoomPanel.styles"
import {ZOOM_MAX, ZOOM_MIN, ZOOM_STEP} from "./ZoomPanel.constants";

interface ZoomPanelProps {
    zoom: number
    zoomSliderHandler: (event: Event, newValue: number | number[]) => void
    onResetZoom: MouseEventHandler
}

export default function ZoomPanel({ zoom, zoomSliderHandler, onResetZoom }: Readonly<ZoomPanelProps>) {
    return (
        <Stack
            display="flex"
            justifyContent="center"
            alignItems="center"
            sx={{
                outline: "1px solid",
                borderRadius: "5px",
                padding: "5px",
                paddingLeft: "10px",
                paddingRight: "10px",
                boxShadow: "3px 3px 3px gray",
                marginRight: "10px"
            }}
        >
            <Typography variant="h6" noWrap component="span" sx={ZoomPanelStyles.text}>
                Zoom: {`${zoom} %`}
            </Typography>
            <Slider
                value={zoom}
                onChange={zoomSliderHandler}
                step={ZOOM_STEP}
                min={ZOOM_MIN}
                max={ZOOM_MAX}
                sx={{ width: "90%", marginTop: "5px", marginBottom: "5px" }}
            />
            <Typography variant="h6" noWrap component="span" sx={ZoomPanelStyles.subText}>
                alt + scroll wheel
            </Typography>
            <Button variant="contained" color="secondary" onClick={onResetZoom} sx={{ width: "100px" }}>
                Reset
            </Button>
        </Stack>
    )
}
