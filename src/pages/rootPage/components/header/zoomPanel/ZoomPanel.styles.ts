const ZoomPanelStyles = {
    text: {
        fontSize: 15,
        fontWeight: "bold",
        color: "text.primary",
        textDecoration: "none"
    },
    subText: {
        fontSize: 13,
        fontWeight: "bold",
        color: "text.primary",
        textDecoration: "none",
        textAlign: "center"
    }
}

export default ZoomPanelStyles
