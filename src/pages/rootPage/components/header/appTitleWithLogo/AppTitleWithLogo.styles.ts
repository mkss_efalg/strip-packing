const AppTitleWithLogoStyles = {
    link: {
        mr: {
            xs: "auto",
            sm: 2
        },
        ml: {
            xs: "auto",
            sm: 0
        },
        display: "flex",
        alignItem: "center"
    },
    logo: {
        height: {
            xs: "100px",
            sm: "100px"
        },
        ":hover": {
            cursor: "pointer",
            boxShadow: "2px 2px 2px 2px lightgray"
        },
        position: "relative",
        top: "-15px",
        mr: 3
    },
    type: {
        mr: {
            xs: "auto",
            sm: 2
        },
        ml: {
            xs: "auto",
            sm: 2
        },
        color: "text.secondary",
        textDecoration: "none"
    },
    title: {
        fontSize: 15,
        fontWeight: "bold",
        mb: {
            xs: "auto",
            sm: 0
        },
        mr: {
            xs: "auto",
            sm: 1
        },
        ml: {
            xs: "auto",
            sm: 1
        },
        color: "text.primary",
        textDecoration: "none"
    },
    subtitle: {
        fontSize: 15,
        mt: {
            xs: "auto",
            sm: 1
        },
        mr: {
            xs: "auto",
            sm: 1
        },
        ml: {
            xs: "auto",
            sm: 1
        },
        color: "text.secondary",
        textDecoration: "none"
    }
}

export default AppTitleWithLogoStyles
