import { Box, SxProps } from "@mui/material"
import LogoImage from "../../../../../../assets/stripPackingLogo.png"

interface AppLogoProps {
    onLogoClick: () => void
    sx?: SxProps
}

export default function AppLogo({ onLogoClick, sx }: Readonly<AppLogoProps>) {
    return <Box onClick={onLogoClick} src={LogoImage} sx={sx} component="img" alt="strip-packing logo" />
}
