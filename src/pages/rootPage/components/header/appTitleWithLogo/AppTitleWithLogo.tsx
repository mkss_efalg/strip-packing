import { Divider, Link, Stack, Typography } from "@mui/material"
import AppTitleWithLogoStyles from "./AppTitleWithLogo.styles"
import AppLogo from "./appLogo/AppLogo"

interface AppTitleWithLogoProps {
    onLogoClick: () => void
}

export default function AppTitleWithLogo({ onLogoClick }: Readonly<AppTitleWithLogoProps>) {
    return (
        <Link underline="none" sx={AppTitleWithLogoStyles.link}>
            <AppLogo onLogoClick={onLogoClick} sx={AppTitleWithLogoStyles.logo} />
            <Divider orientation="vertical" flexItem />
            <Stack alignItems="center" justifyContent="center">
                <Typography variant="h6" component="span" sx={AppTitleWithLogoStyles.title}>
                    strip-packing
                </Typography>
                <Typography variant="subtitle2" component="span" sx={AppTitleWithLogoStyles.subtitle}>
                    Packing optimizer
                </Typography>
            </Stack>
        </Link>
    )
}
