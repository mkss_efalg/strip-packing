import { Button, ButtonGroup, Stack } from "@mui/material"
import React, { MouseEventHandler } from "react"
import PermDataSettingIcon from "@mui/icons-material/PermDataSetting"
import RefreshIcon from "@mui/icons-material/Refresh"
import BackpackIcon from "@mui/icons-material/Backpack"
import InputIcon from "@mui/icons-material/Input"
import OutputIcon from "@mui/icons-material/Output"
import InfoSharpIcon from "@mui/icons-material/InfoSharp"
import { ButtonPanelStyles } from "./ButtonPanel.styles"

interface ButtonPanelProps {
    onStripConfigurationClick: MouseEventHandler
    onGenerateRectanglesClick: () => void
    onAlgorithmDetailsClick: () => void
    handleGenerateRectangles: () => void
    onImportConfigurationChange: (e: any) => void
    onExportConfigurationClick: () => void
    onStartPackingClick: MouseEventHandler
}

export default function ButtonPanel({
    onStripConfigurationClick,
    onGenerateRectanglesClick,
    handleGenerateRectangles,
    onAlgorithmDetailsClick,
    onImportConfigurationChange,
    onExportConfigurationClick,
    onStartPackingClick
}: Readonly<ButtonPanelProps>) {
    const handleGenerateRectanglesClick = () => {
        onGenerateRectanglesClick()
    }

    const handleChange = (e: any) => {
        onImportConfigurationChange(e)
    }

    return (
        <Stack direction="row" spacing={1}>
            <Stack direction="column" spacing={1} marginTop={1} marginBottom={1}>
                <Button variant="contained" color="warning" component="label" endIcon={<InputIcon />} sx={ButtonPanelStyles.button}>
                    {"Import Configuration"}
                    <input type="file" onChange={handleChange} onClick={e => ((e.target as any).value = null)} hidden />
                </Button>
                <Button variant="contained" color="warning" onClick={onExportConfigurationClick} endIcon={<OutputIcon />} sx={ButtonPanelStyles.button}>
                    Export Configuration
                </Button>
                <Button variant="contained" color="info" onClick={onAlgorithmDetailsClick} endIcon={<InfoSharpIcon />} sx={ButtonPanelStyles.button}>
                    Algorithm Details
                </Button>
            </Stack>
            <Stack direction="column" spacing={1} marginTop={1} marginBottom={1}>
                <Button
                    variant="contained"
                    color="secondary"
                    onClick={onStripConfigurationClick}
                    endIcon={<PermDataSettingIcon />}
                    sx={ButtonPanelStyles.button}
                >
                    Strip Configuration
                </Button>
                <ButtonGroup fullWidth variant="contained" color="success" sx={{ boxShadow: "none" }}>
                    <Button variant="contained" color="success" onClick={handleGenerateRectanglesClick} sx={ButtonPanelStyles.button}>
                        Generate Rectangles
                    </Button>
                    <Button
                        variant="contained"
                        color="success"
                        onClick={handleGenerateRectangles}
                        startIcon={<RefreshIcon />}
                        sx={{ width: "1px", minWidth: "1px", maxWidth: "1px", ...ButtonPanelStyles.button }}
                    />
                </ButtonGroup>
                <Button disabled={false} variant="contained" onClick={onStartPackingClick} endIcon={<BackpackIcon />} sx={ButtonPanelStyles.button}>
                    Start Packing
                </Button>
            </Stack>
        </Stack>
    )
}
