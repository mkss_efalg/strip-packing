import { AppBar, Container, Stack, Toolbar } from "@mui/material"
import HeaderStyles from "./Header.styles"
import StripPackingTabs from "./stripPackingTabs/StripPackingTabs"
import AlgorithmChooser from "./algorithmChooser/AlgorithmChooser"
import AppTitleWithLogo from "./appTitleWithLogo/AppTitleWithLogo"
import React, { ChangeEvent, Dispatch, MouseEventHandler, SetStateAction, SyntheticEvent } from "react"
import ZoomPanel from "./zoomPanel/ZoomPanel"
import ButtonPanel from "./buttonPanel/ButtonPanel"
import AlgorithmConfiguration from "./algorithmConfiguration/AlgorithmConfiguration"
import ScalePanel from "./scalePanel/ScalePanel"

interface HeaderProps {
    zoom: number
    zoomSliderHandler: (event: Event, newValue: number | number[]) => void
    onResetZoom: MouseEventHandler
    scale: number
    scaleSliderHandler: (event: Event, newValue: number | number[]) => void
    onResetScale: MouseEventHandler
    algorithm: string
    setAlgorithm: Dispatch<SetStateAction<string>>
    onStripConfigurationClick: MouseEventHandler
    onGenerateRectanglesClick: () => void
    onAlgorithmDetailsClick: () => void
    handleGenerateRectangles: () => void
    onImportConfigurationChange: (e: any) => void
    onExportConfigurationClick: () => void
    onStartPackingClick: MouseEventHandler
    tabIndex: number
    onLogoClick: () => void
    onTabIndexChangeHandler: (e: SyntheticEvent, newValue: number) => void
    handleSteinbergHeuristicChange: (e: ChangeEvent<HTMLInputElement>) => void
    steinbergHeuristic: boolean
    handleCompareAlgorithmsChange: (e: ChangeEvent<HTMLInputElement>) => void
    compareAlgorithms: boolean
    handleDisableVisualizationChange: (e: ChangeEvent<HTMLInputElement>) => void
    disableVisualization: boolean
    handleDisableSteinbergHeuristicChange: (e: ChangeEvent<HTMLInputElement>) => void
    disableSteinbergHeuristic: boolean
    handleEnableSteinbergInverseFirstChange: (e: ChangeEvent<HTMLInputElement>) => void
    enableSteinbergInverseFirst: boolean
}

export default function Header(headerProps: Readonly<HeaderProps>) {
    return (
        <AppBar position="fixed" color="inherit" sx={HeaderStyles.AppBar}>
            <Container maxWidth={false}>
                <Toolbar disableGutters>
                    <Stack direction="row" alignItems="center" flex="max-content" justifyContent="space-between">
                        <Stack alignItems="center" direction="row">
                            <AppTitleWithLogo {...headerProps} />
                            <ZoomPanel {...headerProps} />
                            <ScalePanel {...headerProps} />
                        </Stack>
                        <Stack spacing={2} alignItems="center">
                            <StripPackingTabs {...headerProps} />
                            <AlgorithmConfiguration {...headerProps} />
                        </Stack>
                        <Stack alignItems="center" direction="row">
                            <AlgorithmChooser {...headerProps} />
                            <ButtonPanel {...headerProps} />
                        </Stack>
                    </Stack>
                </Toolbar>
            </Container>
        </AppBar>
    )
}
