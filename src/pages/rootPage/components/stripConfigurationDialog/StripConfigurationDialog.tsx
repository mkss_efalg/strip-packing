import {
    Box,
    Button,
    Checkbox,
    Dialog,
    DialogActions,
    DialogContent,
    DialogContentText,
    DialogTitle,
    Divider,
    FormControlLabel,
    FormGroup,
    InputAdornment,
    Paper,
    PaperProps,
    TextField,
    Tooltip,
    Typography
} from "@mui/material"
import Draggable from "react-draggable"
import React, { ChangeEvent, Dispatch, SetStateAction } from "react"
import WidthNormalIcon from "@mui/icons-material/WidthNormal"
import HeightIcon from "@mui/icons-material/Height"
import HideSourceIcon from "@mui/icons-material/HideSource"
import RestartAltIcon from "@mui/icons-material/RestartAlt"
import { RectangleType } from "../../../../algorithms/Algorithm.types"
import ZoomPanelStyles from "../header/zoomPanel/ZoomPanel.styles"

export const STRIP_WIDTH_MIN = 1
export const STRIP_HEIGHT_MIN = 1

interface StripConfigurationDialogProps {
    open: boolean
    handleClose: () => void
    handleClear: () => void
    handleStripChange: () => void
    stripWidth: number
    setStripWidth: Dispatch<SetStateAction<number>>
    stripHeight: number
    setStripHeight: Dispatch<SetStateAction<number>>
    handleDynamicHeightChange: (e: ChangeEvent<HTMLInputElement>) => void
    dynamicHeight: boolean
    handleEnableLevelLineChange: (e: ChangeEvent<HTMLInputElement>) => void
    enableLevelLine: boolean
    handleEnableSteinbergLegendChange: (e: ChangeEvent<HTMLInputElement>) => void
    enableSteinbergLegend: boolean
    handleEnablePackedHeightChange: (e: ChangeEvent<HTMLInputElement>) => void
    enablePackedHeight: boolean
    handleEnableOptimalHeightChange: (e: ChangeEvent<HTMLInputElement>) => void
    enableOptimalHeight: boolean
    setInputRectangles: Dispatch<SetStateAction<RectangleType[]>>
}

function PaperComponent(props: PaperProps) {
    return (
        <Draggable handle="#strip-configuration-dialog" cancel={'[class*="MuiDialogContent-root"]'}>
            <Paper {...props} />
        </Draggable>
    )
}

export default function StripConfigurationDialog({
    open,
    handleClose,
    handleClear,
    handleStripChange,
    stripWidth,
    setStripWidth,
    stripHeight,
    setStripHeight,
    handleDynamicHeightChange,
    dynamicHeight,
    handleEnableLevelLineChange,
    enableLevelLine,
    handleEnableSteinbergLegendChange,
    enableSteinbergLegend,
    handleEnablePackedHeightChange,
    enablePackedHeight,
    handleEnableOptimalHeightChange,
    enableOptimalHeight,
    setInputRectangles
}: Readonly<StripConfigurationDialogProps>) {
    const handleStripWidth = (e: ChangeEvent<HTMLInputElement>) => {
        const newStripWidth = +e.target.value
        if (newStripWidth >= STRIP_WIDTH_MIN) {
            handleStripChange()
            setInputRectangles([])
            setStripWidth(+e.target.value)
            if (dynamicHeight) {
                setStripHeight(1)
            }
        }
    }

    const handleStripHeight = (e: ChangeEvent<HTMLInputElement>) => {
        const newStripHeight = +e.target.value
        if (newStripHeight >= STRIP_HEIGHT_MIN) {
            handleStripChange()
            setStripHeight(+e.target.value)
        }
    }

    return (
        <Dialog open={open} onClose={handleClose} aria-labelledby="strip-configuration-dialog" PaperComponent={PaperComponent} sx={{ zIndex: 1000 }}>
            <DialogTitle sx={{ cursor: "move" }}>Strip Configuration</DialogTitle>
            <DialogContent>
                <DialogContentText>
                    Enter the width and height of the strip. All input rectangles are cleared whenever any of the parameters are changed.
                </DialogContentText>
                <Box component="form" noValidate autoComplete="off">
                    <TextField
                        onChange={handleStripWidth}
                        autoFocus
                        helperText={`${STRIP_WIDTH_MIN} - ∞`}
                        margin="dense"
                        id="stripWidth"
                        label="Strip width"
                        type="number"
                        value={stripWidth}
                        fullWidth
                        variant="standard"
                        InputProps={{
                            startAdornment: (
                                <InputAdornment position="start">
                                    <WidthNormalIcon />
                                </InputAdornment>
                            )
                        }}
                    />
                    {!dynamicHeight && (
                        <TextField
                            onChange={handleStripHeight}
                            helperText={`${STRIP_HEIGHT_MIN} - ∞`}
                            margin="dense"
                            id="stripHeight"
                            label="Strip height"
                            type="number"
                            value={stripHeight}
                            fullWidth
                            variant="standard"
                            InputProps={{
                                startAdornment: (
                                    <InputAdornment position="start">
                                        <HeightIcon />
                                    </InputAdornment>
                                )
                            }}
                        />
                    )}
                    <FormGroup>
                        <Tooltip
                            title="If enabled the height will not be fixed anymore. Instead the height will be dynamically set to the highest packed rectangle."
                            enterDelay={750}
                            enterNextDelay={750}
                            disableInteractive
                        >
                            <FormControlLabel
                                control={<Checkbox checked={dynamicHeight} onChange={handleDynamicHeightChange} />}
                                label="Enable dynamic height"
                            />
                        </Tooltip>
                    </FormGroup>
                    <Divider />
                    <Typography variant="h6" component="span" sx={{ ...ZoomPanelStyles.text, textAlign: "center" }}>
                        Configuration options:
                    </Typography>
                    <FormGroup>
                        <Tooltip
                            title="Enables the Level Line that is showing the shelf heights for NFDH and FFDH."
                            enterDelay={750}
                            enterNextDelay={750}
                            disableInteractive
                        >
                            <FormControlLabel
                                control={<Checkbox checked={enableLevelLine} onChange={handleEnableLevelLineChange} />}
                                label="Enable Level Line"
                            />
                        </Tooltip>
                        <Tooltip title="Enables legend for steinberg at the side of strip container." enterDelay={750} enterNextDelay={750} disableInteractive>
                            <FormControlLabel
                                control={<Checkbox checked={enableSteinbergLegend} onChange={handleEnableSteinbergLegendChange} />}
                                label="Enable Steinberg Legend"
                            />
                        </Tooltip>
                        <Tooltip
                            title="When enabled, then the height for the packed height of the chosen algorithm will be shown."
                            enterDelay={750}
                            enterNextDelay={750}
                            disableInteractive
                        >
                            <FormControlLabel
                                control={<Checkbox checked={enablePackedHeight} onChange={handleEnablePackedHeightChange} />}
                                label="Enable Packed height"
                            />
                        </Tooltip>
                        <Tooltip
                            title="When enabled, then the height for the optimal height will be shown."
                            enterDelay={750}
                            enterNextDelay={750}
                            disableInteractive
                        >
                            <FormControlLabel
                                control={<Checkbox checked={enableOptimalHeight} onChange={handleEnableOptimalHeightChange} />}
                                label="Enable Optimal height"
                            />
                        </Tooltip>
                    </FormGroup>
                </Box>
            </DialogContent>
            <DialogActions>
                <Button variant="contained" color="secondary" onClick={handleClose} startIcon={<HideSourceIcon />}>
                    Hide
                </Button>
                <Button
                    variant="contained"
                    color="warning"
                    onClick={() => {
                        handleClear()
                        handleClose()
                    }}
                    endIcon={<RestartAltIcon />}
                >
                    Clear Strip
                </Button>
            </DialogActions>
        </Dialog>
    )
}
