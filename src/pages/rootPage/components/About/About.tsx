import { Divider, Stack, Typography } from "@mui/material"
import React from "react"
import AboutStyles from "./About.styles"

export default function About() {
    return (
        <Stack spacing={2} sx={{ textAlign: "left" }}>
            <Typography variant="h1" noWrap component="span" sx={AboutStyles.standard}>
                {"About the 2D Strip-Packing Application"}
            </Typography>
            <Divider />
            <Stack spacing={1}>
                <Typography variant="h2" noWrap component="span" sx={AboutStyles.standard}>
                    {"What is Strip packing"}
                </Typography>
                <Typography variant="h3" noWrap component="span" sx={AboutStyles.standard}>
                    {"In the two-dimensional strip packing problem, a strip is given with a finite width W and infinite height, as well as a set of rectangles. \n" +
                        "The goal is to pack the set of rectangles into the strip while minimizing the packed height. The packed rectangles are not allowed to overlap or be rotated. \n" +
                        "To solve the issue, different algorithms from various authors exist. Five of these algorithms are implemented. All of these are for offline packing. \n" +
                        "An online packing algorithm would account that the set of rectangles arrive over time, and the received rectangle will be placed immediately."}
                </Typography>
            </Stack>
            <Stack spacing={1}>
                <Typography variant="h2" noWrap component="span" sx={AboutStyles.standard}>
                    {"Implemented offline packing algorithms"}
                </Typography>
                <Typography variant="h3" noWrap component="span" sx={AboutStyles.standard}>
                    {"• First-Fit-Decreasing-Height (FFDH) " +
                        "\n• Next-Fit-Decreasing-Height (NFDH) " +
                        "\n• Reserve-Fit (RF) " +
                        "\n• Steinberg (ST) and Steinberg with Drop Packing Heuristic (ST-DP) " +
                        "\n• Kenyon Rémila (KR), not working (implementation missing)"}
                </Typography>
            </Stack>
            <Stack spacing={1}>
                <Typography variant="h2" noWrap component="span" sx={AboutStyles.standard}>
                    {"Features of the application"}
                </Typography>
                <Typography variant="h3" noWrap component="span" sx={AboutStyles.standard}>
                    {"• Adjustable Zooming and Scaling " +
                        "\n• Four working algorithms + a toggleable heuristic for steinberg and configuration for procedure sequence" +
                        "\n• Comparison of algorithms with graphs and indicators (runtime and packed height), also comparable to optimal solution if given " +
                        "\n• Configurations are exportable and importable (JSON-format) " +
                        "\n• All ZDF files can be imported (if visualization is disabled)" +
                        "\n• Strip container is configurable, also possible to enable fixed height => all packed rectangles above the height are marked yellow" +
                        "\n• Generate random rectangles => optimal solution will also be generated" +
                        "\n• Overlay feature (overlay packed solution with optimal solution, also opacity is adjustable)" +
                        "\n• Configurable Indicators for packed height, optimal height, Levels (or shelf heights) and steinberg legend" +
                        "\n• Rectangle-Viewer to view generated or imported rectangles" +
                        "\n• Optimal solution is displayable in rectangle-viewer tab if given" +
                        "\n• Possible to filter and sort the rectangles in rectangle-viewer" +
                        "\n• Rectangles are clickable (Detailed information will pop up in a drawer from the right)" +
                        "\n• Standard browser features like performance graph" +
                        "\n• Not optimized for mobile, but usable" +
                        "\n• Responsive UI" +
                        "\n• Logo is clickable!"}
                </Typography>
            </Stack>
            <Stack spacing={1}>
                <Typography variant="h2" noWrap component="span" sx={AboutStyles.standard}>
                    {"Manual"}
                </Typography>
                <Typography variant="h3" noWrap component="span" sx={AboutStyles.standard}>
                    {"• " +
                        'By clicking on the "Strip Configuration" button a popup will show up in which you can ' +
                        "set a strip width manually and enable dynamic height. Also options like level lining, " +
                        "steinberg legend, packed height and optimal height can be enabled or disabled." +
                        '\n• In the "Generate Rectangles" button you can setup the desired rectangle height and the ' +
                        "number of splits that will be done recursively on the rectangle height defined " +
                        'in "Generate Rectangles" and rectangle width defined in under "Strip Configuration" ' +
                        "the button." +
                        '\n• With a click on the "Import Configuration" Button you are able to import a json ' +
                        "formatted rectangle list or a zdf file. For the json format content you can generate a set " +
                        'of rectangles by a click on the "Generate Rectangles" button and afterwards use ' +
                        'the "Export Configuration" button to export an example json file.' +
                        '\n• Generate or import rectangle and select an algorithm, hit the "Start Packing" button to ' +
                        "start the packing for this algorithm." +
                        "\n• This application can be used to run a single strip packing algorithm or multiple strip " +
                        "packing algorithm to compare the results - like runtime, packed height or to check how much more " +
                        "the algorithm is against the OPT height." +
                        "\n• To find this information you need to first run at least one algorithm or select " +
                        'the "Compare with other Algorithms" checkbox and afterwards click on "Algorithm details" button.' +
                        "\n• In the Strip-packing view on the right hand side are overlay features which contains three buttons. With these you can change the strip packing view and also you can adjust the opacity, if you select the feature which will display the optimum positioning with the algorithm positioning." +
                        "\n• You can also see detailed information about each rectangle packed by clicking on the " +
                        "rectangle in the container. Or to see the list of all packed rectangles you can click on " +
                        'the "Rectangle Viewer" button. Both will display also the size of rectangles that was given.' +
                        "\n• Its also possible to zoom into the container with the Zoom slider in the header or to " +
                        "scale rectangles with the Scale slider which is very helpful for many small packed rectangles." +
                        '\n• Because the default "Steinberg" algorithm is using the fixed height of 2* OPT we have ' +
                        'included the "Steinberg Drop Packing Heuristic" checkbox, which will drop ' +
                        'the "flying" rectangles above each other.' +
                        "\n \n Happy packing !"}
                </Typography>
            </Stack>
            <Stack spacing={1}>
                <Typography variant="h2" noWrap component="span" sx={AboutStyles.standard}>
                    {"Authors"}
                </Typography>
                <Typography variant="h3" noWrap component="span" sx={AboutStyles.standard}>
                    {"Tuncer C. & Andreas K. for HSB master module"}
                </Typography>
            </Stack>
        </Stack>
    )
}
