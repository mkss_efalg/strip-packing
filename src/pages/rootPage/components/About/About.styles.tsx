const AboutStyles = {
    text: {
        fontSize: 15,
        fontWeight: "bold",
        color: "text.primary",
        textDecoration: "none"
    },
    subText: {
        fontSize: 15,
        color: "text.primary",
        textDecoration: "none"
    },
    standard: {
        fontWeight: "bold",
        color: "text.primary",
        whiteSpace: "pre-wrap"
    }
}

export default AboutStyles
