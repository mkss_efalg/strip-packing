import {
    Accordion,
    AccordionDetails,
    AccordionSummary,
    Button,
    Dialog,
    DialogActions,
    DialogContent,
    DialogContentText,
    DialogTitle,
    Divider,
    Paper,
    PaperProps,
    Stack,
    Typography
} from "@mui/material"
import Draggable from "react-draggable"
import React from "react"
import HideSourceIcon from "@mui/icons-material/HideSource"
import AlgorithmDetailsDialogStyles from "./AlgorithmDetailsDialog.styles"
import { BarChart } from "@mui/x-charts"
import { AlgorithmInfo, translateAlgorithm, translateAlgorithmInfo, translateAlgorithmShort } from "../../../../components/Frontend.function"
import ExpandMoreIcon from "@mui/icons-material/ExpandMore"

export const STRIP_WIDTH_MIN = 1
export const STRIP_HEIGHT_MIN = 1

interface StripConfigurationDialogProps {
    open: boolean
    handleClose: () => void
    algorithm: string
    algorithmInfos: AlgorithmInfo[]
    algorithmHeight: number
    optimalHeight: number
    steinbergHeuristic: boolean
    packedRectangleCount: number
}

function PaperComponent(props: PaperProps) {
    return (
        <Draggable handle="#algorithm-details-dialog" cancel={'[class*="MuiDialogContent-root"]'}>
            <Paper {...props} />
        </Draggable>
    )
}

export default function AlgorithmDetailsDialog(props: Readonly<StripConfigurationDialogProps>) {
    const dataLabels = props.algorithmInfos.map(algorithmInfo => translateAlgorithmInfo(algorithmInfo))
    const runTimeData = props.algorithmInfos.map(algorithmInfo => algorithmInfo.runTime!)
    const heightData = props.algorithmInfos.map(algorithmInfo => (Number.isNaN(algorithmInfo.height!) ? 0 : algorithmInfo.height!))
    const thatMuchMoreThanOptData = props.algorithmInfos.map(algorithmInfo =>
        !Number.isNaN(props.optimalHeight) && !Number.isNaN(algorithmInfo.height!)
            ? algorithmInfo.height === undefined
                ? algorithmInfo.height!
                : algorithmInfo.height! / props.optimalHeight
            : 0
    )
    thatMuchMoreThanOptData.push(1)
    if (!Number.isNaN(props.optimalHeight)) {
        heightData.push(props.optimalHeight)
    }
    const valueFormatter = (value: number) => `${value}`
    const algorithmEdited = props.algorithm === "steinberg" && props.steinbergHeuristic ? "steinbergWithDropPacking" : props.algorithm
    return (
        <Dialog open={props.open} onClose={props.handleClose} aria-labelledby="algorithm-details-dialog" PaperComponent={PaperComponent} sx={{ zIndex: 1000 }}>
            <DialogTitle sx={{ cursor: "move" }}>Algorithm Details</DialogTitle>
            <DialogContent>
                <DialogContentText>
                    This dialog will show detailed results to the algorithm that was chosen to be executed. If "Compare with other Algorithms" located in the
                    header is enabled, then the runtime and packed height will be compared.
                </DialogContentText>
                <Stack spacing={2} marginTop={3}>
                    <Stack direction="row" spacing={1}>
                        <Typography variant="h6" component="span" sx={AlgorithmDetailsDialogStyles.text}>
                            Chosen algorithm for packing:
                        </Typography>
                        <Typography variant="h6" component="span" sx={AlgorithmDetailsDialogStyles.subText}>
                            {translateAlgorithm(algorithmEdited) + " (" + translateAlgorithmShort(algorithmEdited) + ")"}
                        </Typography>
                    </Stack>
                    <Stack direction="row" spacing={1}>
                        <Typography variant="h6" component="span" sx={AlgorithmDetailsDialogStyles.text}>
                            Packed Rectangle Count:
                        </Typography>
                        <Typography variant="h6" component="span" sx={AlgorithmDetailsDialogStyles.subText}>
                            {props.packedRectangleCount}
                        </Typography>
                    </Stack>
                    <Divider />
                    <Typography variant="h6" component="span" sx={{ ...AlgorithmDetailsDialogStyles.subText, fontStyle: "italic" }}>
                        Enable "Compare with other Algorithms" located in the header to compare with other Algorithms
                    </Typography>
                    <Accordion>
                        <AccordionSummary expandIcon={<ExpandMoreIcon />} aria-controls="panel1-content" id="panel1-header">
                            Legend
                        </AccordionSummary>
                        <AccordionDetails>
                            <Typography variant="h6" component="span" sx={{ ...AlgorithmDetailsDialogStyles.subText, fontStyle: "italic" }}>
                                <b>NFDH</b> = Next-Fit-Decreasing-Height, <b>FFDH</b> = First-Fit-Decreasing-Height, <b>RF</b> = Reverse-Fit, <b>ST</b> =
                                Steinberg, <b>ST-DP</b> = Steinberg with Drop Packing Heuristic, <b>KR</b> = Kenyon Rémila, <b>OPT</b> = Optimal
                            </Typography>
                        </AccordionDetails>
                    </Accordion>
                    <Stack>
                        <Typography variant="h6" component="span" sx={AlgorithmDetailsDialogStyles.text}>
                            Runtime graph (less is better):
                        </Typography>
                        <BarChart
                            layout="horizontal"
                            xAxis={[{ label: "time (ms)" }]}
                            yAxis={[{ scaleType: "band", data: dataLabels }]}
                            series={[{ data: runTimeData, valueFormatter }]}
                            width={500}
                            height={300}
                        />
                    </Stack>
                    <Stack>
                        <Typography variant="h6" component="span" sx={AlgorithmDetailsDialogStyles.text}>
                            Packed height graph (less is better):
                        </Typography>
                        <BarChart
                            layout="horizontal"
                            xAxis={[{ label: "height" }]}
                            yAxis={[{ scaleType: "band", data: !Number.isNaN(props.optimalHeight) ? [...dataLabels, "OPT"] : dataLabels }]}
                            series={[{ data: heightData, valueFormatter }]}
                            width={500}
                            height={300}
                        />
                    </Stack>
                    {!Number.isNaN(props.optimalHeight) && (
                        <Stack>
                            <Typography variant="h6" component="span" sx={AlgorithmDetailsDialogStyles.text}>
                                'That much more than OPT' graph (less is better):
                            </Typography>
                            <BarChart
                                layout="horizontal"
                                xAxis={[{ label: "That much more than OPT" }]}
                                yAxis={[{ scaleType: "band", data: [...dataLabels, "OPT"] }]}
                                series={[{ data: thatMuchMoreThanOptData, valueFormatter }]}
                                width={500}
                                height={300}
                            />
                        </Stack>
                    )}
                    {Number.isNaN(props.optimalHeight) && (
                        <Stack>
                            <Typography variant="h6" component="span" sx={AlgorithmDetailsDialogStyles.text}>
                                'That much more than OPT' graph (less is better):
                            </Typography>
                            <Typography variant="h6" component="span" sx={{ ...AlgorithmDetailsDialogStyles.subText, fontStyle: "italic" }}>
                                {"<No data was provided for optimal rectangle constellation.>"}
                            </Typography>
                        </Stack>
                    )}
                </Stack>
            </DialogContent>
            <DialogActions>
                <Button variant="contained" color="secondary" onClick={props.handleClose} startIcon={<HideSourceIcon />}>
                    Hide
                </Button>
            </DialogActions>
        </Dialog>
    )
}
