const AlgorithmDetailsDialogStyles = {
    text: {
        fontSize: 15,
        fontWeight: "bold",
        color: "text.primary",
        textDecoration: "none"
    },
    subText: {
        fontSize: 15,
        color: "text.primary",
        textDecoration: "none"
    }
}

export default AlgorithmDetailsDialogStyles
