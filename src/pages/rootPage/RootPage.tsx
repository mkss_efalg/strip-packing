import StripPackingContainer from "../../components/container/StripPackingContainer"
import {
    Button,
    Divider,
    Drawer,
    FormControl,
    Grid,
    IconButton,
    InputAdornment,
    MenuItem,
    Select,
    SelectChangeEvent,
    Slider,
    Stack,
    TextField,
    ToggleButton,
    ToggleButtonGroup,
    Tooltip,
    Typography
} from "@mui/material"
import RootPageStyles from "./RootPage.styles"
import { RectangleType } from "../../algorithms/Algorithm.types"
import Header from "./components/header/Header"
import React, { ChangeEvent, SyntheticEvent, useEffect, useState } from "react"
import CustomTabPanel from "../../components/customTabPanel/CustomTabPanel"
import GetRandomRectanglesBySplitting from "../../components/randomGenerator/GetRandomRectanglesBySplitting"
import StartPackingAlgorithm from "./components/header/algorithmChooser/StartPackingAlgorithm"
import StripConfigurationDialog, { STRIP_WIDTH_MIN } from "./components/stripConfigurationDialog/StripConfigurationDialog"
import GenerateRectanglesDialog from "./components/generateRectanglesDialog/GenerateRectanglesDialog"
import Rectangle from "../../components/Rectangle/Rectangle"
import ViewModuleIcon from "@mui/icons-material/ViewModule"
import ViewQuiltIcon from "@mui/icons-material/ViewQuilt"
import JoinFullIcon from "@mui/icons-material/JoinFull"
import JoinLeftIcon from "@mui/icons-material/JoinLeft"
import GetConfigurationData from "../../components/configuration/GetConfigurationData"
import moment from "moment"
import { ConfigurationType } from "../../components/configuration/Configuration.types"
import { ZOOM_MAX, ZOOM_MIN, ZOOM_STEP } from "./components/header/zoomPanel/ZoomPanel.constants"
import ArrowDropDownIcon from "@mui/icons-material/ArrowDropDown"
import ArrowDropUpIcon from "@mui/icons-material/ArrowDropUp"
import { AlgorithmInfo, filter, getHighestRectangle, importZdf, sort } from "../../components/Frontend.function"
import OptimalRectanglesDialog from "./components/optimalRectanglesDialog/OptimalRectanglesDialog"
import FilterAltIcon from "@mui/icons-material/FilterAlt"
import AbcIcon from "@mui/icons-material/Abc"
import TagIcon from "@mui/icons-material/Tag"
import WidthNormalIcon from "@mui/icons-material/WidthNormal"
import HeightIcon from "@mui/icons-material/Height"
import PreviewIcon from "@mui/icons-material/Preview"
import LayersClearIcon from "@mui/icons-material/LayersClear"
import BorderClearIcon from "@mui/icons-material/BorderClear"
import ClearIcon from "@mui/icons-material/Clear"
import { enqueueSnackbar } from "notistack"
import { INITIAL_SCALE } from "./components/header/scalePanel/ScalePanel.constants"
import ZoomPanelStyles from "./components/header/zoomPanel/ZoomPanel.styles"
import AlgorithmDetailsDialog from "./components/algorithmDetailsDialog/AlgorithmDetailsDialog"
import GetPackedResult from "../../components/configuration/GetPackedResult"
import About from "./components/About/About"

const algorithms: string[] = ["nextFitDecreasingHeight", "firstFitDecreasingHeight", "reverseFit", "steinberg", "steinbergWithDropPacking"]

export default function RootPage() {
    const [tabIndex, setTabIndex] = useState(0)

    const [algorithm, setAlgorithm] = useState("nextFitDecreasingHeight")

    const [heightTotal, setHeightTotal] = useState(-1)

    const [inputRectangles, setInputRectangles] = useState<RectangleType[]>([])
    const [optimalRectangles, setOptimalRectangles] = useState<RectangleType[]>([])
    const [orderedRectangles, setOrderedRectangles] = useState<RectangleType[]>([])
    const [optimalHeight, setOptimalHeight] = useState(0)
    const [algorithmHeight, setAlgorithmHeight] = useState(0)

    const [steinbergHeuristic, setSteinbergHeuristic] = useState(false)
    const [enableSteinbergInverseFirst, setEnableSteinbergInverseFirst] = useState(false)
    const [compareAlgorithms, setCompareAlgorithms] = useState(false)
    const [disableVisualization, setDisableVisualization] = useState(false)
    const [disableSteinbergHeuristic, setDisableSteinbergHeuristic] = useState(false)
    const [dynamicHeight, setDynamicHeight] = useState(true)
    const [enableLevelLine, setEnableLevelLine] = useState(true)
    const [enableSteinbergLegend, setEnableSteinbergLegend] = useState(true)
    const [enablePackedHeight, setEnablePackedHeight] = useState(true)
    const [enableOptimalHeight, setEnableOptimalHeight] = useState(true)

    const [zoom, setZoom] = useState(100)
    const [scale, setScale] = useState(INITIAL_SCALE)

    const [openStripConfiguration, setOpenStripConfiguration] = useState(false)
    const [stripWidth, setStripWidth] = useState(50)
    const [stripHeight, setStripHeight] = useState(1)

    const [openGenerateRectangles, setOpenGenerateRectangles] = useState(false)
    const [rectangleHeight, setRectangleHeight] = useState(50)
    const [numberOfSplits, setNumberOfSplits] = useState(5)

    const [openAlgorithmDetails, setOpenAlgorithmDetails] = useState(false)

    const [view, setView] = useState("packing")

    const [rectangleSort, setRectangleSort] = useState("ascending")
    const [rectangleProperty, setRectangleProperty] = useState("id")
    const [searchRectangle, setSearchRectangle] = useState("")
    const [rectangleSearchProperty, setRectangleSearchProperty] = useState("freeSearch")
    const [openOptimalRectangles, setOpenOptimalRectangles] = useState(false)

    const [opacitySlider, setOpacitySlider] = useState(50)

    const [selectedFile, setSelectedFile] = useState<File | null>(null)

    const [markers, setMarkers] = useState<number[]>([])
    const [algorithmInfos, setAlgorithmInfos] = useState<AlgorithmInfo[]>([])

    const [openDrawer, setOpenDrawer] = useState(false)

    const [drawerText, setDrawerText] = useState("")

    const handleViewChange = (event: React.MouseEvent<HTMLElement>, nextView: string) => {
        if (nextView !== null) {
            setView(nextView)
        }
    }

    const handleRectangleSortChange = (event: React.MouseEvent<HTMLElement>, nextRectangleSort: string) => {
        if (nextRectangleSort !== null) {
            setRectangleSort(nextRectangleSort)
        }
    }
    const handleRectanglePropertyChange = (event: SelectChangeEvent) => {
        setRectangleProperty(event.target.value)
    }
    const handleSearchRectangle = (e: ChangeEvent<HTMLInputElement>) => {
        setSearchRectangle(e.target.value)
    }
    const handleRectangleSearchPropertyChange = (event: SelectChangeEvent) => {
        setRectangleSearchProperty(event.target.value)
    }
    const onRectangleViewReset = () => {
        setRectangleSort("ascending")
        setRectangleProperty("id")
    }
    const onClearRectangles = () => {
        setInputRectangles([])
    }
    const handleOptimalRectanglesOpen = () => {
        setOpenOptimalRectangles(true)
    }
    const handleOptimalRectanglesClose = () => {
        setOpenOptimalRectangles(false)
    }
    const handleClickClearSearchRectangle = () => setSearchRectangle("")

    const handleMouseDownSearchRectangle = (event: React.MouseEvent<HTMLButtonElement>) => {
        event.preventDefault()
    }

    const onTabIndexChange = (e: SyntheticEvent, newValue: number) => {
        setTabIndex(newValue)
    }

    const onLogoClick = () => {
        setTabIndex(0)
    }

    const onStartPacking = () => {
        const startPacking = StartPackingAlgorithm(
            algorithm,
            stripWidth,
            stripHeight,
            inputRectangles,
            steinbergHeuristic,
            enableSteinbergInverseFirst,
            dynamicHeight
        )

        if (startPacking.error.length > 0) {
            enqueueSnackbar(startPacking.error, { variant: "error", preventDuplicate: true })
            handleStripClear()
            return
        }

        setOptimalRectangles(inputRectangles)

        let highestAlgorithmHeight = 0
        const algorithmInfoTmp: AlgorithmInfo[] = []
        if (compareAlgorithms) {
            for (const currentAlgorithm of algorithms) {
                const algoTmp = algorithm === "steinberg" && steinbergHeuristic ? "steinbergWithDropPacking" : algorithm
                if (currentAlgorithm !== algoTmp) {
                    if (disableSteinbergHeuristic && currentAlgorithm === "steinbergWithDropPacking") {
                        continue
                    }
                    const startPackingAlgorithm = StartPackingAlgorithm(
                        currentAlgorithm === "steinbergWithDropPacking" ? "steinberg" : currentAlgorithm,
                        stripWidth,
                        stripHeight,
                        inputRectangles,
                        currentAlgorithm === "steinbergWithDropPacking",
                        enableSteinbergInverseFirst,
                        dynamicHeight
                    )
                    const algorithmHeight = getHighestRectangle(startPackingAlgorithm.sortedRectangles)
                    if (algorithmHeight && currentAlgorithm !== "kenyonRemila") {
                        const tmp = algorithmHeight.posY! + algorithmHeight.height
                        if (tmp > highestAlgorithmHeight) {
                            highestAlgorithmHeight = tmp
                        }
                    }
                    algorithmInfoTmp.push({
                        algorithm: currentAlgorithm,
                        runTime: startPackingAlgorithm.runTime,
                        height: algorithmHeight ? algorithmHeight.posY! + algorithmHeight.height : undefined
                    })
                }
            }
        }

        let warning = false
        const highestRectangle = getHighestRectangle(startPacking.sortedRectangles)
        if (highestRectangle) {
            const heightTotal = highestRectangle.posY! + highestRectangle.height
            algorithmInfoTmp.push({
                algorithm: algorithm === "steinberg" && steinbergHeuristic ? "steinbergWithDropPacking" : algorithm,
                runTime: startPacking.runTime,
                height: heightTotal
            })
            setAlgorithmHeight(heightTotal)
            setAlgorithmInfos(algorithmInfoTmp)
            if (heightTotal > highestAlgorithmHeight) {
                setHeightTotal(heightTotal)
            } else {
                setHeightTotal(highestAlgorithmHeight)
            }
            if (!dynamicHeight && heightTotal > stripHeight) {
                warning = true
            }
            if (algorithm === "nextFitDecreasingHeight" || algorithm === "firstFitDecreasingHeight") {
                setMarkers([...startPacking.markers, heightTotal])
            } else {
                setMarkers([...startPacking.markers])
            }

            if (dynamicHeight) {
                setStripHeight(heightTotal)
            }
            if (disableVisualization) {
                onExportPackingResult(startPacking, heightTotal)
            }
        }

        if (warning) {
            enqueueSnackbar("Some rectangles do not fit into the strip container.", { variant: "warning", preventDuplicate: true })
        } else {
            enqueueSnackbar("Rectangles fit into the strip container.", { variant: "success", preventDuplicate: true })
        }

        setOrderedRectangles(startPacking.sortedRectangles)
    }

    const handleSteinbergHeuristicChange = (e: ChangeEvent<HTMLInputElement>) => {
        localStorage.setItem("steinbergHeuristic", String(e.target.checked))
        setSteinbergHeuristic(e.target.checked)
    }
    const handleEnableSteinbergInverseFirstChange = (e: ChangeEvent<HTMLInputElement>) => {
        localStorage.setItem("enableSteinbergInverseFirst", String(e.target.checked))
        setEnableSteinbergInverseFirst(e.target.checked)
    }
    const handleDisableVisualizationChange = (e: ChangeEvent<HTMLInputElement>) => {
        localStorage.setItem("disableVisualization", String(e.target.checked))
        setDisableVisualization(e.target.checked)
    }
    const handleDisableSteinbergHeuristicChange = (e: ChangeEvent<HTMLInputElement>) => {
        localStorage.setItem("disableSteinbergHeuristicForComparison", String(e.target.checked))
        setDisableSteinbergHeuristic(e.target.checked)
    }
    const handleCompareAlgorithmsChange = (e: ChangeEvent<HTMLInputElement>) => {
        localStorage.setItem("compareAlgorithms", String(e.target.checked))
        setCompareAlgorithms(e.target.checked)
    }

    const handleDynamicHeightChange = (e: ChangeEvent<HTMLInputElement>) => {
        localStorage.setItem("dynamicHeight", String(e.target.checked))
        setDynamicHeight(e.target.checked)
        if (e.target.checked) {
            setStripHeight(1)
            handleStripChange()
        } else {
            setStripHeight(100)
            handleStripChange()
        }
    }
    const handleEnableLevelLineChange = (e: ChangeEvent<HTMLInputElement>) => {
        localStorage.setItem("enableLevelLine", String(e.target.checked))
        setEnableLevelLine(e.target.checked)
    }
    const handleEnableSteinbergLegendChange = (e: ChangeEvent<HTMLInputElement>) => {
        localStorage.setItem("enableSteinbergLegend", String(e.target.checked))
        setEnableSteinbergLegend(e.target.checked)
    }
    const handleEnablePackedHeightChange = (e: ChangeEvent<HTMLInputElement>) => {
        localStorage.setItem("enablePackedHeight", String(e.target.checked))
        setEnablePackedHeight(e.target.checked)
    }
    const handleEnableOptimalHeightChange = (e: ChangeEvent<HTMLInputElement>) => {
        localStorage.setItem("enableOptimalHeight", String(e.target.checked))
        setEnableOptimalHeight(e.target.checked)
    }

    const onImportConfiguration = (e: any) => {
        const file = e.target.files[0]
        setSelectedFile(file)
    }

    const onExportConfiguration = () => {
        const jsonString = JSON.stringify(
            GetConfigurationData(stripWidth, stripHeight, inputRectangles.length === 0 ? optimalRectangles : inputRectangles),
            null,
            2
        )
        const blob = new Blob([jsonString], { type: "application/json" })
        const url = URL.createObjectURL(blob)
        const a = document.createElement("a")
        a.href = url
        a.download = "stripPackingConfigurationExport_" + moment(new Date()).format("MMDDYYYYhmmss") + ".json"
        document.body.appendChild(a)
        a.click()
        document.body.removeChild(a)
        URL.revokeObjectURL(url)
    }

    const onExportPackingResult = (startPacking: any, packedHeight: number) => {
        const jsonString = JSON.stringify(GetPackedResult(stripWidth, packedHeight, startPacking), null, 2)
        const blob = new Blob([jsonString], { type: "application/json" })
        const url = URL.createObjectURL(blob)
        const a = document.createElement("a")
        a.href = url
        a.download = "stripPackingPackedResultExport_" + moment(new Date()).format("MMDDYYYYhmmss") + ".json"
        document.body.appendChild(a)
        a.click()
        document.body.removeChild(a)
        URL.revokeObjectURL(url)
    }

    const handleStripClear = () => {
        setHeightTotal(-1)
        setOptimalRectangles([])
        setOrderedRectangles([])
    }
    const handleStripChange = () => {
        setHeightTotal(-1)
        setOptimalRectangles([])
        setOrderedRectangles([])
    }

    const handleStripConfigurationOpen = () => {
        setOpenStripConfiguration(true)
    }
    const handleStripConfigurationClose = () => {
        setOpenStripConfiguration(false)
    }

    const handleGenerateRectanglesOpen = () => {
        setOpenGenerateRectangles(true)
    }
    const handleGenerateRectanglesClose = () => {
        setOpenGenerateRectangles(false)
    }
    const handleGenerateRectangles = () => {
        const generatedRectanglesTmp = GetRandomRectanglesBySplitting(stripWidth, rectangleHeight, 0, 0, numberOfSplits)
        generatedRectanglesTmp.forEach((inputRectangleTmp, i) => (inputRectangleTmp.id = i))
        setInputRectangles(generatedRectanglesTmp)
        const highestOptRect = getHighestRectangle(generatedRectanglesTmp)
        setOptimalHeight(highestOptRect ? highestOptRect.posY! + highestOptRect.height : 0)
        setOpenGenerateRectangles(false)
    }

    const handleAlgorithmDetailsOpen = () => {
        if (orderedRectangles.length > 0) {
            setOpenAlgorithmDetails(true)
        } else {
            enqueueSnackbar("Start packing algorithm first.", { variant: "error", preventDuplicate: true })
        }
    }
    const handleAlgorithmDetailsClose = () => {
        setOpenAlgorithmDetails(false)
    }

    const handleOpacitySlider = (event: Event, newValue: number | number[]) => {
        setOpacitySlider(newValue as number)
    }

    const handleZoomSlider = (event: Event, newValue: number | number[]) => {
        setZoom(newValue as number)
    }
    const handleScaleSlider = (event: Event, newValue: number | number[]) => {
        setScale(newValue as number)
        localStorage.setItem("scale", String(newValue as number))
    }

    const resetZoom = () => {
        setZoom(100)
    }
    const resetScale = () => {
        setScale(INITIAL_SCALE)
        localStorage.setItem("scale", String(INITIAL_SCALE))
    }

    useEffect(() => {
        const handleWheel = (e: WheelEvent) => {
            if (e.altKey) {
                e.preventDefault()
                setZoom(prevState => {
                    if (e.deltaY > ZOOM_MIN) {
                        const zoomTmp = prevState - ZOOM_STEP
                        if (zoomTmp <= 0) {
                            return prevState
                        }
                        return zoomTmp
                    } else {
                        const zoomTmp = prevState + ZOOM_STEP
                        if (zoomTmp > ZOOM_MAX) {
                            return prevState
                        }
                        return zoomTmp
                    }
                })
            }
        }

        document.addEventListener("wheel", handleWheel, { passive: false })

        const steinbergHeuristicFromStorage = localStorage.getItem("steinbergHeuristic")
        if (steinbergHeuristicFromStorage) {
            setSteinbergHeuristic(JSON.parse(steinbergHeuristicFromStorage))
        }

        const enableSteinbergInverseFirstFromStorage = localStorage.getItem("enableSteinbergInverseFirst")
        if (enableSteinbergInverseFirstFromStorage) {
            setEnableSteinbergInverseFirst(JSON.parse(enableSteinbergInverseFirstFromStorage))
        }

        const compareAlgorithmsFromStorage = localStorage.getItem("compareAlgorithms")
        if (compareAlgorithmsFromStorage) {
            setCompareAlgorithms(JSON.parse(compareAlgorithmsFromStorage))
        }

        const disableVisualizationFromStorage = localStorage.getItem("disableVisualization")
        if (disableVisualizationFromStorage) {
            setDisableVisualization(JSON.parse(disableVisualizationFromStorage))
        }

        const disableSteinbergHeuristicFromStorage = localStorage.getItem("disableSteinbergHeuristicForComparison")
        if (disableSteinbergHeuristicFromStorage) {
            setDisableSteinbergHeuristic(JSON.parse(disableSteinbergHeuristicFromStorage))
        }

        const dynamicHeightFromStorage = localStorage.getItem("dynamicHeight")
        if (dynamicHeightFromStorage) {
            const dynamicHeightFromStorageBool: boolean = JSON.parse(dynamicHeightFromStorage)
            setDynamicHeight(dynamicHeightFromStorageBool)
            setStripHeight(dynamicHeightFromStorageBool ? 1 : 100)
        }

        const enableLevelLineFromStorage = localStorage.getItem("enableLevelLine")
        if (enableLevelLineFromStorage) {
            setEnableLevelLine(JSON.parse(enableLevelLineFromStorage))
        }

        const enableSteinbergLegendFromStorage = localStorage.getItem("enableSteinbergLegend")
        if (enableSteinbergLegendFromStorage) {
            setEnableSteinbergLegend(JSON.parse(enableSteinbergLegendFromStorage))
        }

        const enablePackedHeightFromStorage = localStorage.getItem("enablePackedHeight")
        if (enablePackedHeightFromStorage) {
            setEnablePackedHeight(JSON.parse(enablePackedHeightFromStorage))
        }

        const enableOptimalHeightFromStorage = localStorage.getItem("enableOptimalHeight")
        if (enableOptimalHeightFromStorage) {
            setEnableOptimalHeight(JSON.parse(enableOptimalHeightFromStorage))
        }

        const scaleFromStorage = localStorage.getItem("scale")
        if (scaleFromStorage) {
            setScale(JSON.parse(scaleFromStorage))
        }

        return () => {
            document.removeEventListener("wheel", handleWheel)
        }
    }, [])

    function preventHorizontalKeyboardNavigation(event: React.KeyboardEvent) {
        if (event.key === "ArrowLeft" || event.key === "ArrowRight") {
            event.preventDefault()
        }
    }

    useEffect(() => {
        if (selectedFile) {
            const reader = new FileReader()
            reader.readAsText(selectedFile)
            reader.onloadend = () => {
                try {
                    const configuration: ConfigurationType = JSON.parse(reader.result as string)
                    handleStripClear()
                    setStripWidth(configuration.stripWidth)
                    if (dynamicHeight) {
                        setStripHeight(1)
                    } else {
                        setStripHeight(configuration.stripHeight)
                    }
                    setInputRectangles(configuration.optimalRectangles)
                    const highestOptRect = getHighestRectangle(configuration.optimalRectangles)
                    setOptimalHeight(highestOptRect ? highestOptRect.posY! + highestOptRect.height : 0)

                    enqueueSnackbar("Imported rectangles from JSON file.", { variant: "success", preventDuplicate: true })
                } catch (error) {
                    try {
                        const data = reader.result as string
                        const importedZdf = importZdf(data.split("\r\n"))
                        handleStripClear()
                        if (importedZdf.stripWidth >= STRIP_WIDTH_MIN) {
                            handleStripChange()
                            setStripWidth(importedZdf.stripWidth)
                        } else {
                            throw Error
                        }

                        setStripHeight(importedZdf.stripHeight)
                        setInputRectangles(importedZdf.optimalRectangles)
                        const highestOptRect = getHighestRectangle(importedZdf.optimalRectangles)
                        setOptimalHeight(highestOptRect ? highestOptRect.posY! + highestOptRect.height : 0)

                        enqueueSnackbar("Imported rectangles from ZDF file.", { variant: "success", preventDuplicate: true })
                    } catch (error) {
                        enqueueSnackbar("Could not parse the imported file.", { variant: "error", preventDuplicate: true })
                    }
                } finally {
                    setSelectedFile(null)
                }
            }
        }
    }, [selectedFile])
    const toggleDrawer = (open: boolean) => (event: React.KeyboardEvent | React.MouseEvent) => {
        if (event.type === "keydown" && ((event as React.KeyboardEvent).key === "Tab" || (event as React.KeyboardEvent).key === "Shift")) {
            return
        }

        setOpenDrawer(open)
    }

    const handleOpenDrawer = () => {
        setOpenDrawer(true)
    }

    const rootGridHeight = heightTotal * scale * (zoom / 100) + zoom + 50

    return (
        <Stack spacing={25}>
            <Header
                zoom={zoom}
                zoomSliderHandler={handleZoomSlider}
                onResetZoom={resetZoom}
                scale={scale}
                scaleSliderHandler={handleScaleSlider}
                onResetScale={resetScale}
                algorithm={algorithm}
                setAlgorithm={setAlgorithm}
                onStripConfigurationClick={handleStripConfigurationOpen}
                onGenerateRectanglesClick={handleGenerateRectanglesOpen}
                onAlgorithmDetailsClick={handleAlgorithmDetailsOpen}
                handleGenerateRectangles={handleGenerateRectangles}
                onImportConfigurationChange={onImportConfiguration}
                onExportConfigurationClick={onExportConfiguration}
                onStartPackingClick={onStartPacking}
                tabIndex={tabIndex}
                onLogoClick={onLogoClick}
                onTabIndexChangeHandler={onTabIndexChange}
                handleSteinbergHeuristicChange={handleSteinbergHeuristicChange}
                steinbergHeuristic={steinbergHeuristic}
                compareAlgorithms={compareAlgorithms}
                handleCompareAlgorithmsChange={handleCompareAlgorithmsChange}
                disableVisualization={disableVisualization}
                handleDisableVisualizationChange={handleDisableVisualizationChange}
                handleDisableSteinbergHeuristicChange={handleDisableSteinbergHeuristicChange}
                disableSteinbergHeuristic={disableSteinbergHeuristic}
                handleEnableSteinbergInverseFirstChange={handleEnableSteinbergInverseFirstChange}
                enableSteinbergInverseFirst={enableSteinbergInverseFirst}
            />
            <StripConfigurationDialog
                open={openStripConfiguration}
                handleClose={handleStripConfigurationClose}
                handleClear={handleStripClear}
                handleStripChange={handleStripChange}
                stripWidth={stripWidth}
                setStripWidth={setStripWidth}
                stripHeight={stripHeight}
                setStripHeight={setStripHeight}
                handleDynamicHeightChange={handleDynamicHeightChange}
                dynamicHeight={dynamicHeight}
                handleEnableLevelLineChange={handleEnableLevelLineChange}
                enableLevelLine={enableLevelLine}
                handleEnableSteinbergLegendChange={handleEnableSteinbergLegendChange}
                enableSteinbergLegend={enableSteinbergLegend}
                handleEnablePackedHeightChange={handleEnablePackedHeightChange}
                enablePackedHeight={enablePackedHeight}
                handleEnableOptimalHeightChange={handleEnableOptimalHeightChange}
                enableOptimalHeight={enableOptimalHeight}
                setInputRectangles={setInputRectangles}
            />
            <GenerateRectanglesDialog
                open={openGenerateRectangles}
                handleClose={handleGenerateRectanglesClose}
                handleGenerate={handleGenerateRectangles}
                rectangleHeightMax={stripHeight}
                rectangleHeight={rectangleHeight}
                setRectangleHeight={setRectangleHeight}
                numberOfSplits={numberOfSplits}
                setNumberOfSplits={setNumberOfSplits}
                dynamicHeight={dynamicHeight}
            />
            <OptimalRectanglesDialog
                open={openOptimalRectangles}
                handleClose={handleOptimalRectanglesClose}
                optimalRectangles={inputRectangles}
                optimalHeight={optimalHeight}
                stripWidth={stripWidth}
                scale={scale}
                toggleDrawer={handleOpenDrawer}
                setDrawerText={setDrawerText}
            />
            <AlgorithmDetailsDialog
                open={openAlgorithmDetails}
                handleClose={handleAlgorithmDetailsClose}
                algorithm={algorithm}
                algorithmInfos={algorithmInfos}
                algorithmHeight={algorithmHeight}
                optimalHeight={optimalHeight}
                steinbergHeuristic={steinbergHeuristic}
                packedRectangleCount={orderedRectangles.length}
            />
            <Drawer
                anchor="right"
                open={openDrawer}
                onClose={toggleDrawer(false)}
                PaperProps={{
                    sx: {
                        height: "calc(90% - 200px)",
                        top: 200,
                        marginLeft: "20px",
                        marginRight: "20px",
                        marginTop: "20px",
                        borderRadius: "10px"
                    }
                }}
                sx={{ zIndex: 10000000 }}
            >
                <Typography
                    variant="h6"
                    marginTop="20px"
                    marginLeft="20px"
                    marginRight="20px"
                    component="span"
                    sx={{ ...ZoomPanelStyles.text, whiteSpace: "pre-wrap" }}
                >
                    {drawerText}
                </Typography>
            </Drawer>
            <CustomTabPanel value={tabIndex} index={0}>
                <Stack direction="row">
                    {!disableVisualization && (
                        <Grid
                            maxWidth="xl"
                            container
                            spacing={0}
                            direction="column"
                            alignItems="center"
                            justifyContent="flex-end"
                            marginLeft={0}
                            marginRight={0}
                            marginBottom={0}
                            paddingRight={0}
                            minHeight={rootGridHeight}
                            sx={RootPageStyles.rootStack}
                        >
                            <Grid item xs={1} sx={{ zoom: scale * (zoom / 100) }} marginLeft={4} marginRight={4}>
                                <StripPackingContainer
                                    view={view}
                                    width={stripWidth}
                                    height={stripHeight}
                                    rectangles={orderedRectangles}
                                    optimalRectangles={optimalRectangles}
                                    optimalHeight={optimalHeight}
                                    opacity={opacitySlider}
                                    markers={markers}
                                    toggleDrawer={handleOpenDrawer}
                                    setDrawerText={setDrawerText}
                                    algorithmInfos={algorithmInfos}
                                    heightTotal={heightTotal}
                                    compareAlgorithms={compareAlgorithms}
                                    enableLevelLine={enableLevelLine}
                                    enableSteinbergLegend={enableSteinbergLegend}
                                    enablePackedHeight={enablePackedHeight}
                                    enableOptimalHeight={enableOptimalHeight}
                                />
                            </Grid>
                        </Grid>
                    )}
                    {disableVisualization && (
                        <Grid
                            maxWidth="xl"
                            container
                            spacing={0}
                            direction="column"
                            alignItems="center"
                            justifyContent="flex-start"
                            marginLeft={0}
                            marginRight={0}
                            marginBottom={0}
                            paddingRight={0}
                            minHeight={rootGridHeight}
                            sx={RootPageStyles.rootStack}
                        >
                            <Grid item xs={1} marginLeft={4} marginRight={4}>
                                <Stack sx={{ textAlign: "center" }}>
                                    <Typography
                                        variant="h3"
                                        component="span"
                                        sx={{
                                            fontWeight: "bold",
                                            color: "text.primary",
                                            whiteSpace: "pre-wrap"
                                        }}
                                    >
                                        {
                                            "Visualization is disabled, you can switch it on with the checkbox in the header. \n Packed result will be exported as JSON file instead when starting the packing algorithm."
                                        }
                                    </Typography>
                                </Stack>
                            </Grid>
                        </Grid>
                    )}

                    {!disableVisualization && (
                        <Stack direction="row" spacing={1} sx={{ position: "fixed", right: "50px", zIndex: 800, backgroundColor: "rgb(245,245,245)" }}>
                            {view === "packingAndOptimal" && (
                                <Stack spacing={1} alignItems="center">
                                    <JoinFullIcon />
                                    <Slider
                                        orientation="vertical"
                                        value={opacitySlider}
                                        valueLabelDisplay="auto"
                                        valueLabelFormat={`${opacitySlider} %`}
                                        onKeyDown={preventHorizontalKeyboardNavigation}
                                        onChange={handleOpacitySlider}
                                        sx={{
                                            height: "80px"
                                        }}
                                    />
                                    <JoinLeftIcon />
                                </Stack>
                            )}
                            <ToggleButtonGroup orientation="vertical" value={view} exclusive onChange={handleViewChange}>
                                <ToggleButton value="packing">
                                    <Tooltip title="Packing" enterDelay={750} enterNextDelay={750} disableInteractive>
                                        <ViewQuiltIcon />
                                    </Tooltip>
                                </ToggleButton>
                                <ToggleButton value="optimal">
                                    <Tooltip title="Optimal" enterDelay={750} enterNextDelay={750} disableInteractive>
                                        <ViewModuleIcon />
                                    </Tooltip>
                                </ToggleButton>
                                <ToggleButton value="packingAndOptimal">
                                    <Tooltip title="Packing overlayed with Optimal" enterDelay={750} enterNextDelay={750} disableInteractive>
                                        <Stack direction="row">
                                            <ViewQuiltIcon fontSize="small" />
                                            <ViewModuleIcon fontSize="small" />
                                        </Stack>
                                    </Tooltip>
                                </ToggleButton>
                            </ToggleButtonGroup>
                        </Stack>
                    )}
                </Stack>
            </CustomTabPanel>
            <CustomTabPanel value={tabIndex} index={1}>
                {inputRectangles.length > 0 && !disableVisualization ? (
                    <Stack spacing={1} marginRight={15}>
                        <Stack spacing={10} direction="row" style={{ overflow: "auto" }}>
                            <Button
                                variant="contained"
                                onClick={handleOptimalRectanglesOpen}
                                endIcon={<PreviewIcon />}
                                sx={{ ...RootPageStyles.button, minWidth: "200px" }}
                            >
                                View Optimal Rectangles
                            </Button>
                            <Typography variant="h6" component="span" sx={{ ...ZoomPanelStyles.text, textAlign: "center" }}>
                                Number of Rectangles: {inputRectangles.length}
                            </Typography>
                            <Stack direction="row" alignItems="center" spacing={1}>
                                <TextField
                                    value={searchRectangle}
                                    placeholder="Search..."
                                    onChange={handleSearchRectangle}
                                    InputProps={{
                                        startAdornment: (
                                            <InputAdornment position="start">
                                                <FilterAltIcon />
                                            </InputAdornment>
                                        ),
                                        endAdornment: (
                                            <Stack direction="row">
                                                <InputAdornment position="end">
                                                    <IconButton
                                                        edge="end"
                                                        onClick={handleClickClearSearchRectangle}
                                                        onMouseDown={handleMouseDownSearchRectangle}
                                                        sx={{ ":hover": { color: "red", backgroundColor: "red" } }}
                                                    >
                                                        <ClearIcon />
                                                    </IconButton>
                                                </InputAdornment>
                                            </Stack>
                                        )
                                    }}
                                />
                                <FormControl>
                                    <Select
                                        value={rectangleSearchProperty}
                                        label="RectangleSearchProperty"
                                        onChange={handleRectangleSearchPropertyChange}
                                        sx={{ width: "160px" }}
                                    >
                                        <MenuItem value={"freeSearch"}>
                                            <div style={{ display: "flex", alignItems: "center" }}>
                                                <div>Free Search</div>
                                                <AbcIcon />
                                            </div>
                                        </MenuItem>
                                        <MenuItem value={"id"}>
                                            <div style={{ display: "flex", alignItems: "center" }}>
                                                <div>Id</div>
                                                <TagIcon />
                                            </div>
                                        </MenuItem>
                                        <MenuItem value={"width"}>
                                            <div style={{ display: "flex", alignItems: "center" }}>
                                                <div>Width</div>
                                                <WidthNormalIcon />
                                            </div>
                                        </MenuItem>
                                        <MenuItem value={"height"}>
                                            <div style={{ display: "flex", alignItems: "center" }}>
                                                <div>Height</div>
                                                <HeightIcon />
                                            </div>
                                        </MenuItem>
                                    </Select>
                                </FormControl>
                            </Stack>
                            <Divider orientation="vertical" />
                            <Stack direction="row" alignItems="center" spacing={1}>
                                <ToggleButtonGroup value={rectangleSort} exclusive onChange={handleRectangleSortChange}>
                                    <ToggleButton value="ascending">
                                        <Tooltip title="Ascending" enterDelay={750} enterNextDelay={750} disableInteractive>
                                            <ArrowDropUpIcon />
                                        </Tooltip>
                                    </ToggleButton>
                                    <ToggleButton value="descending">
                                        <Tooltip title="Descending" enterDelay={750} enterNextDelay={750} disableInteractive>
                                            <ArrowDropDownIcon />
                                        </Tooltip>
                                    </ToggleButton>
                                </ToggleButtonGroup>
                                <FormControl>
                                    <Select
                                        value={rectangleProperty}
                                        label="RectangleProperty"
                                        onChange={handleRectanglePropertyChange}
                                        sx={{ width: "110px" }}
                                    >
                                        <MenuItem value={"id"}>
                                            <div style={{ display: "flex", alignItems: "center" }}>
                                                <div>Id</div>
                                                <TagIcon />
                                            </div>
                                        </MenuItem>
                                        <MenuItem value={"width"}>
                                            <div style={{ display: "flex", alignItems: "center" }}>
                                                <div>Width</div>
                                                <WidthNormalIcon />
                                            </div>
                                        </MenuItem>
                                        <MenuItem value={"height"}>
                                            <div style={{ display: "flex", alignItems: "center" }}>
                                                <div>Height</div>
                                                <HeightIcon />
                                            </div>
                                        </MenuItem>
                                    </Select>
                                </FormControl>
                            </Stack>
                            <Button
                                variant="contained"
                                color="secondary"
                                endIcon={<LayersClearIcon />}
                                onClick={onRectangleViewReset}
                                sx={{ ...RootPageStyles.button, minWidth: "100px" }}
                            >
                                Reset sort
                            </Button>
                            <Button
                                variant="contained"
                                color="secondary"
                                endIcon={<BorderClearIcon />}
                                onClick={onClearRectangles}
                                sx={{ ...RootPageStyles.button, minWidth: "150px" }}
                            >
                                Clear Rectangles
                            </Button>
                        </Stack>
                        <Divider sx={{ width: "100%", paddingTop: "30px" }} />
                        <Stack
                            spacing={0.5}
                            paddingBottom={1}
                            marginRight={1}
                            useFlexGap
                            flexWrap="wrap"
                            direction="row"
                            sx={{ zoom: scale * (zoom / 100), overflow: "auto" }}
                        >
                            {inputRectangles
                                .toSorted(sort(rectangleProperty, rectangleSort))
                                .filter(rectangle => filter(rectangle, searchRectangle, rectangleSearchProperty))
                                .map(inputRectangle => (
                                    <Rectangle
                                        key={inputRectangle.id}
                                        rectangle={{ ...inputRectangle, posX: undefined, posY: undefined }}
                                        backgroundColor="lightgray"
                                        borderColor="gray"
                                        opacity={1}
                                        toggleDrawer={handleOpenDrawer}
                                        setDrawerText={setDrawerText}
                                        total={inputRectangles.length}
                                    />
                                ))}
                        </Stack>
                    </Stack>
                ) : (
                    <Stack sx={{ textAlign: "center" }}>
                        <Typography
                            variant="h3"
                            noWrap
                            component="span"
                            sx={{
                                fontWeight: "bold",
                                color: "text.primary",
                                whiteSpace: "pre-wrap"
                            }}
                        >
                            {disableVisualization
                                ? "Visualization is disabled, you can switch it on with the checkbox in the header. \n Number of Rectangles: " +
                                  inputRectangles.length
                                : "No rectangles to inspect, generate or import rectangles."}
                        </Typography>
                    </Stack>
                )}
            </CustomTabPanel>
            <CustomTabPanel value={tabIndex} index={2}>
                <About />
            </CustomTabPanel>
        </Stack>
    )
}
