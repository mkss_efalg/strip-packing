const RootPageStyles = {
    rootStack: {
        height: "100%",
        maxHeight: "100%",
        width: "100%",
        maxWidth: "100%",
        pb: 6,
        overflow: "auto"
    },
    content: {
        mt: 2
    },
    button: {
        textOverflow: "ellipsis",
        overflow: "hidden",
        whiteSpace: "nowrap"
    }
}

export default RootPageStyles
